# edgecuts

This repository contains the complete implementation necessary to reproduce the
simulations and experiments done for the ICWSM 2020 paper "Measuring Edge
Sparsity on Large Social Networks". If you use this work, please cite:

```bibtex
@paper{smith_edgecuts_2020,
    author = {J. David Smith and My T. Thai},
    title = {Measuring Edge Sparsity on Large Social Networks},
    conference = {International AAAI Conference on Web and Social Media},
    year = {2020},
    keywords = {},
}
```

## Setup

You will need a bit of external software to get going. First, install
[Rust](https://rustup.rs) (mandatory) and [Poetry](https://python-poetry.org)
(optional, but recommended). Use `rustup` to install Stable Rust (this was
written with 2018 Edition).

You may need to install a couple of libraries for `python-igraph` to build,
depending on your exact OS. YMMV.

## Building the Implementation

    cargo build --release

This will download all the Rust dependencies, install, and build a release
binary. It can be run independently of the experiments using the
`./target/release/dump-weights` binary. Use the `--help` flag to see options.

## Running the Experiments

First, edit the `luigi.cfg` file and set the `threads` resource to be the
number of threads you want to limit the experiments to run with. The default is
8.

To get into a shell with the appropriate Python dependencies set up, use:

    poetry shell

This should drop you into a new shell using a virtualenv named something like
`edgecuts-????????-py3.7` where the `?`s may vary. The following command can be
used to generate all of the non-Gephi figures:

    python -m luigi --local-scheduler --module tasks PublishedResults

The experiments may take a long time to run.

### A Note About Twitter

The 2010 Twitter dataset from Kwak et. al. 2010 is not included in this
repository, but can be downloaded from [the author's
page](http://an.kaist.ac.kr/traces/WWW2010.html). **One task will fail** in the
above command unless you download this dataset and place it in
`data/mono/twitter-big.txt`. To preserve storage space, you may `gzip`
it---though obviously running the simulations will take longer if you do.

## Running the Twitter Experiment

If you want to run the same Twitter experiment (i.e. calculating EWs on Twitter) as I did in the paper, you will need to add your own Twitter API key(s) to `twitter-crawler/keys.json`. The format should look something like this:

```json
[
    {
        "consumer_key": "...",
        "consumer_secret": "...",
        "access_token_key": "...",
        "access_token_secret": "..."
    }
]
```

If you have multiple keys, the code will use them as available.

To re-run the experiments, run:

    python -m twitter-crawler 1000 --output test.csv --rho 0.2 --root 2435105154 --root 874808698606170113 --root 854354900280528896 --root 890344663290216448

The first parameter controls the maximum number of weights calculated, while repeatable `--root` parameter controls the starting point of each MHRW. Use the `--help` flag to see the other options. The roots above are the ones used in the paper.

## Organization

You may notice that there are many directories here. Briefly, they are:

- [edgecuts](./edgecuts/): The main implementation of the Edgecut Weight method.
- [edge-betweenness](./edge-betweenness/): My parallel implementation of Brandes' algorithm for Edge Betweenness and the Riondato & Kornaropoulos approximation for it.
- [graph-parse](./graph-parse): A graph parser that accepts a couple of variations on the edgelist format.
- [data](./data/mono): (Most) data used for this project.
- [dump-weights](./dump-weights): Wrapper binary for Edgecuts/Betweenness that runs them on an input graph and dumps the weights to a file.
- [tasks](./tasks/weights.py): Implementation of the plotting / table generation methods for the paper.
- [twitter-crawler](./twitter-crawler): The crawler used to complete the experiments done via the Twitter API.
- [notebooks](./notebooks): A couple of notebooks used for this paper.

Additionally, there are a couple of relevant files: `twitter_2019_weights.csv` and `twitter_2019_weights_0.1.csv`. These are the output of my experiments on Twitter with the `twitter-crawler` program. If you want to run such experiments yourself, you need to add your own Twitter API key(s) to `twitter-crawler/keys.json`.

## License

Copyright 2019-2020 J. David Smith.

See [here](LICENSE.md) for further details.
