import cProfile
import os
import pstats

import igraph
import luigi
import numpy as np
import pandas as pd
import pyximport
pyximport.install()
from luigi import ExternalTask, LocalTarget
from luigi.contrib.external_program import ExternalProgramTask
from mizani.breaks import extended_breaks
from plotnine import *
from sklearn.metrics import (accuracy_score, f1_score, precision_score,
                             recall_score)
from tqdm import tqdm

from stat_ecdf_interp import stat_ecdf_interp

from .constants import *
from .ecdf import *
from .louvain_shuffle import LouvainShufflePart_Set
from .mixins import (DictOutputMixin, RustBacktraceMixin, TaskParameter,
                     TempPathMixin, ThreadsMixin, TimedMixin)
from .theme import full_theme, half_theme


class DumpWeights_Impl(ExternalTask):
    def output(self):
        return LocalTarget('target/release/dump-weights')


class DumpWeights_Base(TimedMixin, ThreadsMixin, RustBacktraceMixin, TempPathMixin,
                       DictOutputMixin, ExternalProgramTask):
    TMP_KEY = "output"
    _OUTPUT_PATH = "results/weights/{}/{{}}.txt"
    _TIMER_PATH = "results/weight_times/{}/{{}}.time"
    _LOG_PATH = "results/weight_logs/{}/{{}}.json"
    threads = luigi.IntParameter(default=8)
    key = luigi.Parameter()
    graph = luigi.Parameter()
    weight_kind = ''

    def __init__(self, *args, **kwargs):
        self.LOG_PATH = self._LOG_PATH.format(self.weight_kind)
        self.TIMER_PATH = self._TIMER_PATH.format(self.weight_kind)
        self.OUTPUT_PATH = self._OUTPUT_PATH.format(self.weight_kind)
        super().__init__(*args, **kwargs)

    def output(self, omit=set()):
        outs = super().output(omit)
        outs['output'] = luigi.LocalTarget(self.OUTPUT_PATH.format(super().log_path(omit)))

        return outs

    def requires(self):
        return {
            'impl': DumpWeights_Impl(),
        }

    def program_args(self):
        args = [
            self.input()['impl'].path,
            self.graph_path(),
            self._tmp_output,
            '--threads',
            self.threads,
            '--log',
            self.output()['log'].path,
            *self.reweight_args(),
        ]

        return self.timed([str(arg) for arg in args])

    def reweight_args(self):
        raise NotImplementedError

    def graph_path(self):
        path = 'data/mono/{}.txt'.format(self.graph)
        if not os.path.exists(path):
            path += '.gz'
        assert os.path.exists(path)
        return path

    def run(self):
        for v in self.output().values():
            v.makedirs()

        super().run()

    def results(self):
        import pandas as pd
        assert self.complete()
        return pd.read_csv(self.output()['output'].path, header=None, names=('source', 'target', 'weight'), engine='c',
                           dtype={'source': np.int64, 'target': np.int64, 'weight': np.float64}, delim_whitespace=True, memory_map=True)


class Dump_BetweennessWeights(DumpWeights_Base):
    weight_kind = "exact-betweenness"

    def reweight_args(self):
        return [self.weight_kind]


class Dump_EdgecutWeights(DumpWeights_Base):
    weight_kind = "edgecut-approx"
    rho = luigi.Parameter(default="0.2")
    delta = luigi.Parameter()
    epsilon = luigi.Parameter()

    def reweight_args(self):
        return [self.weight_kind, self.rho, self.epsilon, self.delta]


class Dump_RiondatoWeights(DumpWeights_Base):
    weight_kind = "approx-betweenness"
    epsilon = luigi.Parameter()
    delta = luigi.Parameter()
    p = luigi.Parameter(default=0.01)

    def reweight_args(self):
        return [self.weight_kind, self.epsilon, self.delta, self.p]


class Meta_WeightData(DictOutputMixin, luigi.WrapperTask):
    key = luigi.Parameter()
    graph = luigi.Parameter()
    epsilon = luigi.Parameter()
    delta = luigi.Parameter()
    rho = luigi.Parameter()
    exclude = luigi.ListParameter(default=[])
    repetitions = luigi.IntParameter()

    def requires(self):
        default = {
            "btw": [Dump_BetweennessWeights(key=self.key, graph=self.graph)],
            "riondato": [Dump_RiondatoWeights(key=self.key + ('-{}'.format(rep) if rep > 0 else ''), graph=self.graph,
                                              epsilon=self.epsilon,
                                              delta=self.delta)
                         for rep in range(self.repetitions)],
            "edgecuts": [Dump_EdgecutWeights(key=self.key + ('-{}'.format(rep) if rep > 0 else ''), graph=self.graph,
                                             epsilon=self.epsilon,
                                             delta=self.delta,
                                             rho=self.rho)
                         for rep in range(self.repetitions)],
        }

        # pylint: disable=unsupported-membership-test
        return {k: v for k, v in default.items()
                if k not in self.exclude}

    def output(self):
        return None


class Correlations(DictOutputMixin, luigi.Task):
    LOG_PATH = "results/correlations/{}.txt"
    data = TaskParameter(kind=Meta_WeightData)
    method = luigi.ChoiceParameter(choices=['spearman', 'kendall'])

    def requires(self):
        return self.data

    def run(self):
        import pandas as pd
        import itertools as it

        results = [
            (res1[0], res2[0], res2[1].runtime() / res1[1].runtime(), Correlations.cor(self.method, res1[1].results()['weight'], res2[1].results()['weight']))
            for res1, res2 in it.combinations(self.data.requires().items(), 2)
        ]

        eps = self.data.epsilon
        delta = self.data.delta

        results = pd.DataFrame([
            (eps, delta, a, b, res.correlation, res.pvalue, rr)
            for a, b, rr, res in results
        ], columns=["epsilon", "delta", "a", "b", "correlation", "pvalue", "runtime_ratio"])

        with self.output().open('w') as out:
            results.to_csv(out, sep='\t', index=False)

    @staticmethod
    def cor(method, weights_a, weights_b):
        from scipy import stats
        if method == 'spearman':
            return stats.spearmanr(weights_a, weights_b)
        elif method == 'kendall':
            return stats.kendalltau(weights_a, weights_b)


class Test_Correlations(luigi.WrapperTask):
    key = luigi.Parameter()
    graph = luigi.Parameter()
    epsilon = luigi.Parameter()
    delta = luigi.Parameter(default='0.01')
    exclude_edgecuts = luigi.BoolParameter(default=False)

    def requires(self):
        return Correlations(data=Meta_WeightData(key=self.key,
                                                 graph=self.graph,
                                                 epsilon=self.epsilon,
                                                 delta=self.delta,
                                                 exclude=['edgecuts'] if self.exclude_edgecuts else []),
                            method='spearman')

class MultiCorrelationTable(DictOutputMixin, luigi.Task):
    LOG_PATH = "results/tables/correlations_{}.tex"
    key = luigi.Parameter()
    epsilons = luigi.ListParameter(default=["0.2", "0.1"])
    delta = luigi.Parameter(default="0.01")
    graphs = luigi.ListParameter(default=["ca-GrQc", "nethept", "email-Enron", "soc-Slashdot0902", "com-dblp.ungraph"])
    rho = luigi.Parameter(default="0.2")
    repetitions = luigi.IntParameter(default=10)

    def requires(self):
        return {
            epsilon: {
                graph: Meta_WeightData(
                    key=self.key,
                    graph=graph,
                    epsilon=epsilon,
                    delta=self.delta,
                    rho=self.rho,
                    repetitions=self.repetitions,
                )
                for graph in self.graphs
            } for epsilon in self.epsilons
        }

    def load_data(self):
        return {
            epsilon: {
                graph: {
                    method: [
                        {
                            "data": task.results(),
                            "runtime": task.runtime(),
                            "wallclock": task.wallclock(),
                        }
                        for task in tasklist
                    ] 
                    for method, tasklist in meta.requires().items()
                }
                for graph, meta in graphs.items()
            } for epsilon, graphs in self.requires().items()
        }


    def run(self):
        import pandas as pd

        rows = [
            (
                rep,
                epsilon,
                graph, 
                method,
                Correlations.cor('kendall', resultset['btw'][0]['data']['weight'], results['data']['weight']).correlation,
                resultset['btw'][0]['wallclock'] / results['wallclock'],
            )
            for epsilon, graphs in self.load_data().items()
            for graph, resultset in graphs.items()
            for method in ["edgecuts", "riondato"]
            for rep, results in enumerate(resultset[method])
        ]

        df = pd.DataFrame(rows, columns=["rep", "epsilon", "dataset", "method", "tau", "speedup"])

        #  pivoted = pd.pivot_table(df, index=["epsilon", "dataset"], columns='method').swaplevel(axis=1).reindex(self.graphs, level='dataset').reindex(self.epsilons, level='epsilon').reindex(['edgecuts', 'riondato'], axis='columns', level='method')
        means = df.groupby(['epsilon', 'dataset', 'method'])[['tau', 'speedup']].mean()
        devs = df.groupby(['epsilon', 'dataset', 'method'])[['tau', 'speedup']].std()
        pivoted = pd.concat([means, devs], keys=['mean', 'stdev'], names=['kind'])
        pivoted = pivoted.unstack(level='kind').unstack(level='method').swaplevel('method', 0, axis=1).reindex(['edgecuts', 'riondato'], axis='columns', level='method').reindex(self.graphs, level='dataset').reindex(self.epsilons, level='epsilon')

        self.output().makedirs()
        with self.output().open('w') as out:
            out.write(pivoted.to_latex(
                float_format=r'\num{{{}}}'.format,
                escape=False,
                multicolumn=True,
            ))



class Extrema(DictOutputMixin, luigi.Task):
    LOG_PATH = "results/tables/extrema_{}.tex"
    key = luigi.Parameter()
    epsilons = luigi.ListParameter(default=["0.2"])
    delta = luigi.Parameter(default="0.01")
    rho = luigi.Parameter(default="0.2")
    graphs = luigi.ListParameter(default=["ca-GrQc", "musae_facebook"])

    def requires(self):
        return {
            epsilon: {
                graph: Meta_WeightData(
                    key=self.key,
                    graph=graph,
                    epsilon=epsilon,
                    rho=self.rho,
                    delta=self.delta,
                    repetitions=1,
                )
                for graph in self.graphs
            } for epsilon in self.epsilons
        }

    def load_data(self):
        return {
            epsilon: {
                graph: {
                    method: task.results()
                    for method, tasks in meta.requires().items()
                    for task in tasks
                }
                for graph, meta in graphs.items()
            } for epsilon, graphs in self.requires().items()
        }

    @staticmethod
    def scores(df_a, df_b):
        assert((df_a[['source', 'target']] == df_b[['source', 'target']]).all().all())
        r_a = df_a['weight'] < df_a['weight'].median()
        r_b = df_b['weight'] < df_b['weight'].median()
        return ( f1_score(r_a.values, r_b.values),
                 accuracy_score(r_a.values, r_b.values),
                 precision_score(r_a.values, r_b.values),
                 recall_score(r_a.values, r_b.values) )



    def run(self):
        import pandas as pd

        rows = [
            (
                epsilon,
                graph, 
                method,
                *Extrema.scores(results['btw'], results[method]),
            )
            for epsilon, graphs in self.load_data().items()
            for graph, results in graphs.items()
            for method in ["edgecuts", "riondato"]
            if method in results
        ]

        df = pd.DataFrame(rows, columns=["epsilon", "dataset", "method", "f1", "accuracy", "precision", "recall"])
        df = pd.melt(df, id_vars=['epsilon', 'dataset', 'method'], value_vars=["f1", "accuracy", "precision", "recall"])
        print(df)

        pivoted = pd.pivot_table(df, index=["epsilon", "dataset", "variable"], columns='method').reindex(self.graphs, level='dataset').reindex(self.epsilons, level='epsilon').reindex(['edgecuts', 'riondato'], axis='columns', level='method')

        with self.output().open('w') as out:
            out.write(pivoted.to_latex(
                float_format=r'\num{{{}}}'.format,
                escape=False,
                multicolumn=True,
            ))


class JaccardTable(DictOutputMixin, luigi.Task):
    LOG_PATH = "results/tables/jaccard_{}.tex"
    key = luigi.Parameter()
    epsilons = luigi.ListParameter(default=["0.2", "0.1"])
    delta = luigi.Parameter(default="0.01")
    graphs = luigi.ListParameter(default=["soc-Slashdot0902", "com-dblp.ungraph"])
    pcts = luigi.ListParameter(default=[0.01, 0.05, 0.1, 0.2])

    def requires(self):
        return {
            epsilon: {
                graph: Meta_WeightData(
                    key=self.key,
                    graph=graph,
                    epsilon=epsilon,
                    delta=self.delta,
                    repetitions=1,
                    rho=0.2
                )
                for graph in self.graphs
            } for epsilon in self.epsilons
        }

    def load_data(self):
        return {
            epsilon: {
                graph: {
                    method: {
                        "data": task.results(),
                        "runtime": task.runtime(),
                        "wallclock": task.wallclock(),
                    }
                    for method, tasks in meta.requires().items()
                    for task in tasks
                }
                for graph, meta in graphs.items()
            } for epsilon, graphs in self.requires().items()
        }

    @staticmethod
    def p_jaccard(weights_a, weights_b, p, kind='tail'):
        from math import ceil
        length = ceil(len(weights_a) * p)
        assert(kind in ['head', 'tail'])
        a = set(getattr(weights_a.sort_values('weight'), kind)(length).apply(lambda row: (int(row['source']), int(row['target'])), axis='columns'))
        b = set(getattr(weights_b.sort_values('weight'), kind)(length).apply(lambda row: (int(row['source']), int(row['target'])), axis='columns'))

        return len(a & b) / len(a | b)


    def run(self):
        import pandas as pd

        rows = [
            (
                epsilon,
                graph, 
                method,
                pct,
                kind,
                JaccardTable.p_jaccard(results['btw']['data'], results[method]['data'], pct, kind),
            )
            for epsilon, graphs in self.load_data().items()
            for graph, results in graphs.items()
            for method in ["edgecuts", "riondato"]
            if method in results
            for kind in ['head', 'tail']
            for pct in self.pcts
        ]

        df = pd.DataFrame(rows, columns=["epsilon", "dataset", "method", "pct", "kind", "jaccard"])

        pivoted = pd.pivot_table(df, index=["epsilon", "dataset", "kind", "pct"], columns='method').reindex(self.graphs, level='dataset').reindex(self.epsilons, level='epsilon').reindex(['edgecuts', 'riondato'], axis='columns', level='method')

        with self.output().open('w') as out:
            out.write(pivoted.to_latex(
                float_format=r'\num{{{}}}'.format,
                escape=False,
                multicolumn=True,
            ))


class Weights_LogPlot(DictOutputMixin, luigi.Task):
    LOG_PATH="results/figs/log_weights_{}.pdf"
    key = luigi.Parameter()
    graph = luigi.Parameter()
    epsilon = luigi.Parameter()
    delta = luigi.Parameter()
    rho = luigi.Parameter(default="0.2")
    method = luigi.ChoiceParameter(choices=['btw', 'edgecuts', 'riondato'])

    def requires(self):
        return {
            'btw': Dump_BetweennessWeights(key=self.key, graph=self.graph),
            'edgecuts': Dump_EdgecutWeights(key=self.key, graph=self.graph, epsilon=self.epsilon, rho=self.rho, delta=self.delta),
            'riondato': Dump_RiondatoWeights(key=self.key, graph=self.graph, epsilon=self.epsilon, delta=self.delta)
        }[self.method]

    def run(self):
        data = self.requires().results()

        dist = pd.Series(np.linspace(1e-6, 1, 200), name='step').to_frame()
        dist['count'] = np.zeros_like(dist['step'], dtype='int64')

        ecdf(dist['step'].values, data['weight'].values, dist['count'].values)

        dist['frac'] = dist['count'] / data.shape[0]

        dist = dist[dist['count'] > 0]

        p = ggplot(dist, aes('step', 'frac')) + geom_step() + full_theme + ylab('Fraction of Edges') + scale_x_log10(name='Edgecut Weight')
        self.output().makedirs()
        p.save(self.output().path)


class Weights_LinPlot(DictOutputMixin, luigi.Task):
    LOG_PATH="results/figs/lin_weights_{}.pdf"
    key = luigi.Parameter()
    graph = luigi.Parameter()
    epsilon = luigi.Parameter()
    delta = luigi.Parameter()
    rho = luigi.Parameter(default="0.2")
    method = luigi.ChoiceParameter(choices=['btw', 'edgecuts', 'riondato'])

    def requires(self):
        return {
            'btw': Dump_BetweennessWeights(key=self.key, graph=self.graph),
            'edgecuts': Dump_EdgecutWeights(key=self.key, graph=self.graph, epsilon=self.epsilon, rho=self.rho, delta=self.delta),
            'riondato': Dump_RiondatoWeights(key=self.key, graph=self.graph, epsilon=self.epsilon, delta=self.delta)
        }[self.method]

    def run(self):
        data = self.requires().results()

        dist = pd.Series(np.linspace(1e-6, 1, 200), name='step').to_frame()
        dist['count'] = np.zeros_like(dist['step'], dtype='int64')

        ecdf(dist['step'].values, data['weight'].values, dist['count'].values)

        dist['frac'] = dist['count'] / data.shape[0]

        dist = dist[dist['count'] > 0]

        p = ggplot(dist, aes('step', 'frac')) + geom_step() + full_theme + ylab('Fraction of Edges') + xlab('Edgecut Weight') + xlim(0, 1)
        self.output().makedirs()
        p.save(self.output().path)


class Weights_LinHistPlot(DictOutputMixin, luigi.Task):
    LOG_PATH="results/figs/lin_hist_{}.pdf"
    key = luigi.Parameter()
    graph = luigi.Parameter()
    epsilon = luigi.Parameter()
    delta = luigi.Parameter()
    rho = luigi.Parameter(default="0.2")
    method = luigi.ChoiceParameter(choices=['btw', 'edgecuts', 'riondato'])

    def requires(self):
        return {
            'btw': Dump_BetweennessWeights(key=self.key, graph=self.graph),
            'edgecuts': Dump_EdgecutWeights(key=self.key, graph=self.graph, epsilon=self.epsilon, rho=self.rho, delta=self.delta),
            'riondato': Dump_RiondatoWeights(key=self.key, graph=self.graph, epsilon=self.epsilon, delta=self.delta)
        }[self.method]

    def run(self):
        data = self.requires().results()

        p = ggplot(data, aes('weight', 'stat(count)')) + stat_bin(binwidth=0.025) + full_theme + ylab(r'\# of Edges') + xlab('Edgecut Weight') + xlim(0, 1)
        self.output().makedirs()
        p.save(self.output().path)


class Weights_Sampled_BySource(DictOutputMixin, luigi.Task):
    LOG_PATH="results/intermediate/weight_samples_bysource_{}.csv"
    key = luigi.Parameter()
    graph = luigi.Parameter()
    epsilon = luigi.Parameter(default="0.2")
    delta = luigi.Parameter(default="0.01")
    rho = luigi.Parameter(default="0.2")
    nsamples = luigi.IntParameter(default=16)
    seed = luigi.IntParameter(default=None)

    def requires(self):
        return Dump_EdgecutWeights(key=self.key, graph=self.graph, epsilon=self.epsilon, rho=self.rho, delta=self.delta)


    def run(self):
        data = self.requires().results()

        state = np.random.RandomState(seed=self.seed)
        sample_nodes = state.choice(data['source'].unique(), self.nsamples, replace=False)
        sample_data = []
        for node in sample_nodes:
            df = data[(data['source'] == node) | (data['target'] == node)].copy()
            df['group'] = str(node)
            sample_data.append(df)

        sample_data = pd.concat(sample_data)

        self.output().makedirs()
        sample_data.to_csv(self.output().path)


class Weights_Sampled_BySource_Plot(Weights_Sampled_BySource):
    LOG_PATH="results/figs/weight_samples_bysource_{}.pdf"

    def requires(self):
        return Weights_Sampled_BySource(key=self.key, graph=self.graph, epsilon=self.epsilon, rho=self.rho, delta=self.delta,
                                        nsamples=self.nsamples, seed=self.seed)

    def run(self):
        df = pd.read_csv(self.input().path)

        groups = df['group'].unique()
        top_groups = groups[:self.nsamples//2]
        bottom_groups = groups[self.nsamples//2:]

        df.loc[df['group'].isin(top_groups), 'row'] = 'top'
        df.loc[df['group'].isin(bottom_groups), 'row'] = 'bottom'

        df['group'] = df['group'].astype(pd.CategoricalDtype(sorted(df['group'].unique()), ordered=True))

        p = ggplot(df, aes(x=0, y='weight')) + geom_boxplot(width=0.45) + geom_rug(sides='r', length=0.12) + facet_wrap('~ group', ncol=self.nsamples//2) +\
            full_theme + xlab('Node') + ylab('Edgecut Weight') + xlim(-0.5, 0.5) +\
            theme(figure_size=(3.4, 3.2), 
                  axis_text_x=element_blank(),
                  axis_title_x=element_blank(),
                  axis_ticks_major_x=element_blank(),
                  axis_ticks_minor_x=element_blank(),
                  strip_background=element_blank(),
                  strip_text_x=element_blank())
        self.output().makedirs()
        p.save(self.output().path)


class Weights_LinTwitterPlot(DictOutputMixin, luigi.Task):
    LOG_PATH="results/figs/lin_weights_twitter_{}.pdf"
    key = luigi.Parameter()
    epsilon = luigi.Parameter()
    delta = luigi.Parameter()

    def requires(self):
        return {
            "0.05": Dump_EdgecutWeights(key=self.key, graph='twitter-big', epsilon=self.epsilon, rho="0.05", delta=self.delta, threads=70),
            "0.2": Dump_EdgecutWeights(key=self.key, graph='twitter-big', epsilon=self.epsilon, rho="0.2", delta=self.delta, threads=36),
        }

    def run(self):
        dists = []
        for rho, task in self.requires().items():
            data = task.results()
            dist = pd.Series(np.linspace(1e-6, 1, 200), name='step').to_frame()
            dist['count'] = np.zeros_like(dist['step'], dtype='int64')

            ecdf(dist['step'].values, data['weight'].values, dist['count'].values)

            dist['frac'] = 1.0 - dist['count'] / data.shape[0]

            dist['rho'] = rho
            dists.append(dist)

        df = pd.concat(dists)
        p = ggplot(df, aes('step', 'frac', color='rho', linetype='rho')) + geom_step() + full_theme + ylab('Fraction of Edges') + xlab('Edgecut Weight') +\
            xlim(0, 1) + scale_color_brewer(type='qual', palette='Set2') + theme(legend_position=(0.2, 0.6))
        self.output().makedirs()
        p.save(self.output().path)


class Weights_Twitter_Reciprocity(DictOutputMixin, luigi.Task):
    LOG_PATH="results/tables/reciprocity.csv"

    def requires(self):
        return Dump_EdgecutWeights(key="dump-update", graph='twitter-big', epsilon="0.2", rho="0.05", delta="0.01", threads=70)

    def run(self):
        from scipy.stats import mannwhitneyu
        graph_path = 'data/mono/twitter-big.txt'
        graph = pd.read_csv(graph_path, header=None, names=('source', 'target', 'weight'), engine='c', skiprows=1,
                           dtype={'source': np.int64, 'target': np.int64, 'weight': np.float64}, delim_whitespace=True, memory_map=True, low_memory=False)

        recip, fwd = calc_recip(graph['source'].values, graph['target'].values)

        print(len(recip), len(fwd), len(graph))
        del graph
        del fwd
        weights = self.requires().results()
        print('loaded weights, matching reciprocated edges...')

        pos, neg = recip_weights(recip, weights['source'].values, weights['target'].values, weights['weight'].values)

        print('matched edges...')
        print('mean', 'std', 'min', 'max')
        print(np.mean(pos), np.std(pos), np.min(pos), np.max(pos))
        print(np.mean(neg), np.std(neg), np.min(neg), np.max(neg))

        print('running tests...')
        print(mannwhitneyu(pos, neg, alternative='two-sided'))
        print(mannwhitneyu(pos, neg, alternative='less'))
        print(mannwhitneyu(pos, neg, alternative='greater'))

        dist = pd.Series(np.linspace(1e-6, 1, 200), name='step').to_frame()
        dist['count'] = np.zeros_like(dist['step'], dtype='int64')
        dist['label'] = 'pos'

        ecdf(dist['step'].values, np.array(pos), dist['count'].values)

        dist['frac'] = dist['count'] / len(pos)

        neg_dist = pd.Series(np.linspace(1e-6, 1, 200), name='step').to_frame()
        neg_dist['count'] = np.zeros_like(neg_dist['step'], dtype='int64')
        neg_dist['label'] = 'neg'

        ecdf(neg_dist['step'].values, np.array(neg), neg_dist['count'].values)

        neg_dist['frac'] = neg_dist['count'] / len(neg)

        df = pd.concat([dist, neg_dist])

        self.output().makedirs()
        df.to_csv(self.output().path, sep='\t', index=False)
        

class Weights_Twitter_ReciprocityPlot(DictOutputMixin, luigi.Task):
    LOG_PATH="results/figs/twitter_reciprocity.pdf"

    def requires(self):
        return Weights_Twitter_Reciprocity()

    def run(self):
        dist = pd.read_csv(self.input().path, sep='\t')
        dist['label'] = dist['label'].replace({'pos': 'Reciprocated', 'neg': 'Unreciprocated'})

        p = ggplot(dist, aes('step', 'frac', color='label', linetype='label')) + geom_step() + full_theme + ylab('Fraction of Edges') + scale_x_continuous(name='Edgecut Weight') + theme(legend_position=(0.3, 0.6))
        self.output().makedirs()
        p.save(self.output().path)


class Weights_MultiPlot(DictOutputMixin, luigi.Task):
    LOG_PATH="results/figs/multi_log_weights_{}_method={}.pdf"
    key = luigi.Parameter()
    graphs = luigi.ListParameter(default=["ca-GrQc", "email-Enron", 'soc-Slashdot0902', 'com-dblp.ungraph'])
    epsilon = luigi.Parameter()
    delta = luigi.Parameter()
    rho = luigi.Parameter(default="0.2")

    def requires(self):
        return {
            graph: {
                'btw': Dump_BetweennessWeights(key=self.key, graph=graph),
                'edgecuts': Dump_EdgecutWeights(key=self.key, graph=graph, epsilon=self.epsilon, rho=self.rho, delta=self.delta)
            }
            for graph in self.graphs
        }

    def load_df(self):
        return pd.DataFrame.from_records([{'graph': graph, 'method': method, 'weight': w}
                                          for graph, methods in self.requires().items()
                                          for method, task in methods.items()
                                          for w in task.results()['weight']])

    def output(self):
        return {
            method: luigi.LocalTarget(self.LOG_PATH.format(self.log_path(), method))
            for method in ['btw', 'edgecuts']
        }

    def run(self):
        df = self.load_df()

        print(df)

        df['graph'].replace(graph_names, inplace=True)

        df['graph'] = df['graph'].astype(pd.CategoricalDtype([graph_names[g] for g in self.graphs], ordered=True))

        for key, kdf in df.groupby('method'):
            kdf.loc[kdf['weight'] == 0, 'weight'] = kdf[kdf['weight'] > 0]['weight'].min()
            p = ggplot(kdf, aes('weight', color='graph', linetype='graph')) + stat_ecdf() +\
                full_theme + scale_x_log10() + ylab('Fraction of Edges') + xlab(labels[key]) +\
                scale_color_brewer(type='qual', palette='Set2') + theme(legend_position=(0.2, 0.6))
            self.output()[key].makedirs()
            p.save(self.output()[key].path)


class Weights_RuntimePlot(DictOutputMixin, luigi.Task):
    LOG_PATH="results/figs/runtime_{}.pdf"
    key = luigi.Parameter()
    graphs = luigi.ListParameter(default=["ca-GrQc", 'nethept', "email-Enron", 'soc-Slashdot0902', 'com-dblp.ungraph', 'btc-1k', 'com-orkut.ungraph'])
    epsilon = luigi.Parameter(default="0.2")
    delta = luigi.Parameter(default="0.01")
    rho = luigi.Parameter(default="0.2")
    repetitions = luigi.IntParameter(default=10)

    def requires(self):
        return {
            graph: {
                'edgecuts': [Dump_EdgecutWeights(key=self.key + ('-{}'.format(rep) if rep > 0 else ''), graph=graph, epsilon=self.epsilon, rho=self.rho, delta=self.delta)
                             for rep in range(self.repetitions)],
                'riondato': [Dump_RiondatoWeights(key=self.key + ('-{}'.format(rep) if rep > 0 else ''), graph=graph, epsilon=self.epsilon, delta=self.delta)
                             for rep in range(self.repetitions)] if graph in safe_btw_graphs else [Dump_RiondatoWeights(key=self.key, graph=graph, epsilon=self.epsilon, delta=self.delta)],
                **({'btw': [Dump_BetweennessWeights(key=self.key, graph=graph)]} if graph in safe_btw_graphs else {})
            }
            for graph in self.graphs
        }

    def run(self):
        rows = [(graph, method, i, task.wallclock())
                for graph, ms in self.requires().items()
                for method, tasks in ms.items()
                for i, task in enumerate(tasks)]

        df = pd.DataFrame(rows, columns=['graph', 'method', 'iter', 'runtime'])

        _labels = dict(**labels)
        _labels['btw'] = 'Brandes'

        df['graph'] = df['graph'].replace(graph_names).astype(pd.CategoricalDtype([graph_names[g] for g in self.graphs], ordered=True))
        df['method'].replace(_labels, inplace=True)
        df['runtime'] = pd.to_timedelta(df['runtime'], unit='s')

        data = df.groupby(['graph', 'method'])['runtime'].mean()
        data['std'] = df.groupby(['graph', 'method'])['runtime'].std()
        print(data)

        p = ggplot(data, aes('graph', 'runtime', color='method', shape='method', group='method')) + geom_ribbon(aes(ymin='runtime - std', ymax='runtime + std'), fill='band') + full_theme + theme(legend_position=(0.3,0.6), axis_text_x=element_text(rotation=45), axis_title_x=element_blank()) + ylab('Running Time') + scale_color_brewer(type='qual', palette='Set2')
        self.output().makedirs()
        p.save(self.output().path)


class Weights_EpsilonSensitivity(DictOutputMixin, luigi.Task):  
    LOG_PATH="results/figs/epsilon_{}.pdf"
    key = luigi.Parameter()
    graph = luigi.Parameter(default="ca-GrQc")
    epsilons = luigi.ListParameter(default=["0.8", "0.4", "0.2", "0.1", "0.05", "0.02", "0.01"])
    reference = luigi.Parameter(default="0.01")
    delta = luigi.Parameter(default="0.01")
    rho = luigi.Parameter(default="0.2")
    repetitions = luigi.IntParameter(default=10)

    def requires(self):
        eps = {
            eps: [Dump_EdgecutWeights(key=self.key + ('-{}'.format(rep) if rep > 0 else ''), graph=self.graph, epsilon=eps, delta=self.delta, rho=self.rho)
                  for rep in range(self.repetitions)]
            for eps in self.epsilons
        }
        eps['ref'] = Dump_EdgecutWeights(key=self.key, graph=self.graph, epsilon=self.reference, delta=self.delta, rho=self.rho)

        return eps

    def run(self):
        from scipy import stats
        ref = self.requires()['ref'].results()

        mse = pd.DataFrame([{'eps': eps, 'iter': i, 'index': j, 'pct_err': p}
                            for eps, tasks in self.requires().items()
                            if eps != "ref"
                            for i, task in enumerate(tasks)
                            for j, p in enumerate((task.results()['weight'] - ref['weight']) / ref['weight'])])

        print(mse)

        mse['eps'] = mse['eps'].apply(float)

        def eps_sum(ser):
            return pd.DataFrame([{
                'ymax': ser.quantile(0.99),
                'upper': ser.quantile(0.75),
                'middle': ser.quantile(0.5),
                'lower': ser.quantile(0.25),
                'ymin': ser.quantile(0.01)
            }])

        p = ggplot(mse, aes('eps', 'pct_err')) +\
            geom_line(aes(y='-eps'), color='red', linetype='-.') +\
            stat_summary(aes(group='eps'), fun_data=eps_sum, geom="boxplot") +\
            full_theme + scale_x_reverse(name=r'$\epsilon$') + ylab(r'Relative Error')
        self.output().makedirs()
        p.save(self.output().path)



class Weight_LinECDF(DictOutputMixin, luigi.Task):
    LOG_PATH="results/figs/weight_ecdf_{}.pdf"
    graphs = luigi.ListParameter()
    epsilon = luigi.Parameter(default='0.2')
    delta = luigi.Parameter(default='0.01')
    key = luigi.Parameter()
    rhos = luigi.ListParameter(default=['0.2', '0.1', '0.05'])

    def requires(self):
        return {
            g: {rho: Dump_EdgecutWeights(key=self.key, epsilon=self.epsilon, delta=self.delta, rho=rho, graph=g)
                for rho in self.rhos}
            for g in self.graphs
        }

    @staticmethod
    def calculate_dist(weight_df, steps=200):
        dist = pd.Series(np.linspace(1e-6, 1, 200), name='step').to_frame()
        dist['count'] = np.zeros_like(dist['step'], dtype='int64')

        ecdf(dist['step'].values, weight_df['weight'].values, dist['count'].values)
        dist['frac'] = dist['count'] / weight_df.shape[0]
        return dist

    def load_weights(self, set_categories=True):
        df = None
        for g, rhos in self.requires().items():
            for rho, task in rhos.items():
                current = task.results()

                dist = Weight_LinECDF.calculate_dist(current)
                dist['rho'] = rho
                dist['Graph'] = graph_names[g]
                del current

                if df is None:
                    df = dist
                else:
                    df = pd.concat([df, dist], ignore_index=True)
                    del dist
        if set_categories:
            df['Graph'] = df['Graph'].astype(pd.CategoricalDtype([graph_names[g] for g in self.graphs], ordered=True))
            df['rho'] = df['rho'].astype(pd.CategoricalDtype(self.rhos, ordered=True))
        return df

    def run(self):
        weights = self.load_weights()
        print(weights)

        guide = guide_legend(title=r'$\rho$')

        p = ggplot(weights, aes('step', 'frac', color='rho', linetype='rho')) + geom_step() + full_theme +\
            xlab('Edgecut Weight') + ylab('Fraction of Edges') + xlim(0, 1) +\
            scale_color_brewer(type='qual', palette='Set2') +\
            facet_wrap("~ Graph", ncol=2) + guides(color=guide, linetype=guide) +\
            theme(
                legend_position=(0.1, 0.9) if len(self.graphs) >= 3 else (0.1, 0.6), 
                figure_size=(3.4, 1.6 * max(len(self.graphs) // 2, 1)), 
                panel_spacing=0.02, 
                panel_background=element_blank(),
                strip_text=element_text(size=8),
                strip_background=element_blank(),
                legend_title=element_text(size=10, ha='center'),
                legend_title_align='center',
                legend_text=element_text(size=8),
                legend_key_height=1.5,
            )
        self.output().makedirs()
        p.save(self.output().path)

class Weight_LinECDF_PlusTwitter(Weight_LinECDF):
    LOG_PATH="results/figs/weight_ecdf_plustwitter_{}.pdf"

    def load_weights(self):
        df = super().load_weights(set_categories=False)
        for rho, path in [("0.2", "twitter_2019_weights.csv"), ("0.1", "twitter_2019_weights_0.1.csv")]:
            # 2706726890,2169503389,0.9602422987793077,1.0,0.0,47
            current = pd.read_csv(path, header=None, names=("source", "target", "weight", "mean", "variance", "count"),
                                  dtype={'source': np.int64, 'target': np.int64, 'weight': np.float64})
            dist = Weight_LinECDF.calculate_dist(current)
            dist['rho'] = rho
            dist['Graph'] = 'Twitter (Estimated, 2019)'

            df = pd.concat([df, dist], ignore_index=True)
        df['Graph'] = df['Graph'].astype(pd.CategoricalDtype([graph_names[g] for g in self.graphs] + ['Twitter (Estimated, 2019)'], ordered=True))
        df['rho'] = df['rho'].astype(pd.CategoricalDtype(self.rhos, ordered=True))
        return df

class Weight_BreakdownRho(DictOutputMixin, luigi.Task):
    LOG_PATH="results/figs/weight_breakdown_rho_{}.pdf"
    graph = luigi.Parameter()
    epsilon = luigi.Parameter(default='0.2')
    delta = luigi.Parameter(default='0.01')
    key = luigi.Parameter()
    rhos = luigi.ListParameter(default=['0.2', '0.1', '0.05'])
    draw_as = luigi.ChoiceParameter(default='hist', choices=['hist', 'ecdf'])

    def requires(self):
        return {
            rho: Dump_EdgecutWeights(
                key=self.key,
                epsilon=self.epsilon,
                delta=self.delta,
                rho=rho,
                graph=self.graph
            )
            for rho in self.rhos
        }

    def load_weights(self):
        return {
            k: t.results()
            for k, t in self.requires().items()
        }

    def run(self):
        weights = pd.concat(self.load_weights())
        weights = weights.reset_index(level=0).rename(columns={'level_0': 'rho'})
        weights['rho'] = weights['rho'].astype(pd.CategoricalDtype(self.rhos, ordered=True))
        
        if self.draw_as == 'hist':
            stat = ggplot(weights, aes('weight', y='stat(density)')) + stat_bin(binwidth=0.01) + facet_grid("rho ~ .")
        else:
            legend = guide_legend(title='$\\rho$')
            stat = ggplot(weights, aes('weight', color='rho', linetype='rho')) + stat_ecdf_interp() + scale_color_brewer(type='qual', palette='Set2') + guides(color=legend, linetype=legend)


        p = stat + half_theme +\
            scale_y_continuous(name='Density', breaks=extended_breaks(n=4)) +\
            xlab('Edgecut Weight') + xlim(0, 1) +\
            theme(
                legend_title=element_text(size=10, ha='center'),
                legend_title_align='center',
                legend_text=element_text(size=8),
                legend_key_height=1.5,
                legend_position=(0.25, 0.6),
                panel_spacing_y=0.08,
                axis_text_y=element_text(size=8),
                strip_text=element_blank(),
                strip_background=element_blank(),
                plot_margin=0
            )

        self.output().makedirs()
        p.save(self.output().path)


class Weight_BreakdownCuts(DictOutputMixin, luigi.Task):
    LOG_PATH="results/figs/weight_breakdown_cuts_{}.pdf"
    graph = luigi.Parameter()
    epsilon = luigi.Parameter(default='0.2')
    delta = luigi.Parameter(default='0.01')
    key = luigi.Parameter()
    rho = luigi.Parameter(default='0.2')
    method = luigi.ChoiceParameter(choices=['louvain']) # this used to support metis as well, i stripped it out
    npart = luigi.IntParameter(default=None)
    no_repetitions = luigi.BoolParameter(default=False)
    draw_as = luigi.ChoiceParameter(default='hist', choices=['hist', 'ecdf'])

    def requires(self):
        assert self.npart is None or self.method == 'metis'
        return {
            "weights": Dump_EdgecutWeights(
                key=self.key,
                epsilon=self.epsilon,
                delta=self.delta,
                rho=self.rho,
                graph=self.graph
            ),
            "cuts": LouvainShufflePart_Set(graph=self.graph, npart=self.npart)
        }

    def load_weights(self):
        weights = self.requires()['weights'].results()
        partitions = [task.partitions() for task in self.requires()['cuts'].requires()]

        if not self.no_repetitions:
            results = []
            for part in partitions:
                part_arr = np.zeros(max(int(k) for k in part.keys()) + 1)
                for k, p in part.items():
                    part_arr[int(k)] = p
                results.append(weights[weights[['source', 'target']].apply(lambda row: part_arr[row['source']] != part_arr[row['target']], axis=1)])
            print([len(r) for r in results])
            cut = pd.concat(results)
        else:
            weights['cut'] = 0
            for part in partitions:
                part_arr = np.zeros(max(int(k) for k in part.keys()) + 1)
                for k, p in part.items():
                    part_arr[int(k)] = p
                weights['cut'] += weights[['source', 'target']].apply(lambda row: int(part_arr[row['source']] != part_arr[row['target']]), axis=1)
            cut = weights[weights['cut'] > 0]

        return {
            "base": weights,
            "cut": cut
        }

    def run(self):
        weights = pd.concat(self.load_weights())
        weights = weights.reset_index(level=0).rename(columns={'level_0': 'kind'})
        weights['kind'] = weights['kind'].astype(pd.CategoricalDtype(['base', 'cut'], ordered=True))

        if self.draw_as == 'hist':
            stat = ggplot(weights, aes('weight', y='stat(density)')) + stat_bin(binwidth=0.01) + facet_grid("kind ~ .", scales="free_y") + half_theme 
        else:
            stat = ggplot(weights, aes('weight', linetype='kind', color='kind')) +\
                stat_ecdf_interp() + scale_color_brewer(type='qual', palette='Set2') +\
                half_theme + theme(legend_position="none")

        p = stat +\
            scale_y_continuous(name='Density', breaks=extended_breaks(n=4)) +\
            xlab('Edgecut Weight') + xlim(0, 1) +\
            theme(
                panel_spacing_y=0.08,
                axis_text_y=element_text(size=8),
                strip_text=element_blank(),
                strip_background=element_blank(),
                plot_margin=0
            )

        self.output().makedirs()
        p.save(self.output().path)

class Weight_Summary(DictOutputMixin, luigi.Task):
    LOG_PATH="results/tables/summaries/weights_{}.tsv"
    graph = luigi.Parameter()
    epsilon = luigi.Parameter(default='0.2')
    delta = luigi.Parameter(default='0.01')
    key = luigi.Parameter()
    rho = luigi.Parameter(default='0.2')

    def requires(self):
        return Dump_EdgecutWeights(key=self.key, epsilon=self.epsilon, delta=self.delta, rho=self.rho, graph=self.graph)

    def load_weights(self):
        return self.requires().results()

    def run(self):
        from scipy import stats
        weights = self.load_weights()
        print(weights)

        self.output().makedirs()

        summary = weights.groupby('source')['weight'].agg(
            count=len,
            mean=np.mean,
            variance=np.var,
            skewness=stats.skew
        )

        print(summary)

        with self.output().open('w') as f:
            summary.fillna(0.0).to_csv(f, sep='\t')

class PublishedResults(luigi.WrapperTask):
    KEY='published'

    def requires(self):
        return [
            # Figure 1 --- this is drawn with networkx in a notebook. See notebooks/TODO
            # Figure 2 (top)
            *[
                Weight_BreakdownRho(
                    key=self.KEY,
                    graph=g,
                    draw_as='ecdf'
                )
                for g in ['ca-GrQc', 'soc-Slashdot0902', 'gg_valid', 'musae_facebook']
            ],
            # Figure 2 (bottom)
            *[
                Weight_BreakdownCuts(
                    key=self.KEY,
                    graph=g,
                    method='louvain',
                    draw_as='ecdf'
                )
                for g in ['ca-GrQc', 'soc-Slashdot0902', 'gg_valid', 'musae_facebook']
            ],
            # Table 1 is hand-written
            # Figure 3
            Weights_EpsilonSensitivity(key=self.KEY),
            # Figure 4
            Weights_RuntimePlot(key=self.KEY),
            # Table 2 --- the output of this requires a little reformatting for publication
            MultiCorrelationTable(key=self.KEY, graphs=['ca-GrQc', 'soc-Slashdot0902', 'gg_valid', 'musae_facebook', 'com-dblp.ungraph']),
            # Table 3
            JaccardTable(
                key=self.KEY,
                pcts=["0.1"],
                epsilons=["0.2"]
            ),
            # Figure 5
            Weight_LinECDF(
                key=self.KEY,
                graphs=['ca-GrQc', 'ca-HepPh', 'nethept', 'com-dblp.ungraph']
            ),   
            # Figure 6a --- 6b is from Gephi
            Weights_LinHistPlot(key=self.KEY, method='edgecuts', graph='nethept', epsilon=0.2, delta=0.01, rho=0.05),
            # Figure 7 --- the full twitter dataset is NOT included because it is too large
            # if you would like to run this without downloading that dataset, simply delete it from the list
            Weight_LinECDF_PlusTwitter(
                key=self.KEY,
                graphs=['soc-Slashdot0902', 'com-orkut.ungraph', 'twitter-big']
            ),
            # Figure 8 is from Gephi
            # Figure 9
            Weight_LinECDF(
                key=self.KEY,
                graphs=['gg_valid', 'email-Enron', 'email-EU-inout', 'wiki-Talk']
            ),
        ]
