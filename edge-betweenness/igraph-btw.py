#!/usr/bin/env python
"""A simple script to generate edge betweenness weights from igraph for
validating the output of this library. Always interprets edges as undirected.

Usage:
    ./igraph-btw.py <input> <output> [--overwrite]
    ./igraph-btw.py (-h | --help)

Options:
    -h --help       Show this information.
    --overwrite     Overwrite <output>.
"""
from igraph import Graph
from docopt import docopt
import os
import sys

if __name__ == '__main__':
    args = docopt(__doc__)

    if not args['--overwrite'] and os.path.exists(args['<output>']):
        print("Output file exists! Pass --overwrite to overwrite it. Exiting")
        sys.exit(1)
        

    with open(args['<input>'], 'r') as f:
        # the format i use is not one igraph parses by default
        edges = [[int(v) for v in line.split()] for line in f]

    # first line is node / edge count
    n, m = edges[0][0], edges[0][1]

    # igraph wants continuous node labels from 0-n
    labels = {}
    ix = 0

    def label(x):
        global ix
        if x not in labels:
            labels[x] = ix
            ix += 1
        return labels[x]
    
    edges = [(label(e[0]), label(e[1])) for e in edges[1:]]
    existing = set()
    final = []
    for edge in edges:
        e = (min(*edge), max(*edge))
        if e not in existing:
            existing.add(e)
            final.append(e)
    print(len(final))
    g = Graph()
    g.add_vertices(ix)
    g.add_edges(final)

    btw = g.edge_betweenness(directed=False)

    scale = g.vcount() * (g.vcount() - 1)

    with open(args['<output>'], 'w') as f:
        for b in btw:
            # igraph divides the weights by 2 for undirected graphs...ugh
            # it also doesn't rescale by n(n-1), which my code does.
            # this *should* make the numbers match
            f.write(f'{b * 2 / scale}\n')