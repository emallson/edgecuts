# Edge Betweenness

This library contains the code to calculate Edge Betweenness (publication
pending; [author version](http://emallson.net/pubs/icwsm20_edgecuts.pdf)) from
offline network data. It contains both implementations of Brandes' method and
the Riondato & Kornaropoulos approximation scheme that can calculate weights in
a parallel manner.

## Use

To use this as a library in your own code, add the following to your `Cargo.toml`

```toml
[dependencies.edge-betweenness]
git = "https://gitlab.com/emallson/edgecuts.git
```

## Citing

If you use this software, please cite the following paper: 

```bibtex
@paper{smith_edgecuts_2020,
    author = {J. David Smith and My T. Thai},
    title = {Measuring Edge Sparsity on Large Social Networks},
    conference = {International AAAI Conference on Web and Social Media},
    year = {2020},
    keywords = {},
}
```

## Implementation Notes

Parallelism can be controlled by configuring [rayon](https://docs.rs/rayon),
which is used here. While the R&K implementation is lock-free (using
`AtomicUsize` instead), the Brandes implementation is not due to a lack of
`AtomicF64` and me not wanting to make a fatal mistake trying to implement it.
Nonetheless, Brandes' method spends the bulk of its time in the Single-Source
Shortest-Path step that does not touch the shared mutable state, so contention
was not a problem in my tests (using 8 threads).

Additionally, this implementation of Brandes' method is slower than the igraph
implementation. The exact ratio depends on the graph being used as the main
bottleneck lies in the underlying graph data structure I used for this project,
which was originally written for another project and adapted to this---with
some inherited wonkiness. With 4 threads, you should see similar performance to
igraph. If you can go higher than that, you start to see gains.

Nonetheless, if I were to use this in future work I'd certainly replace the
graph data structure used with one more optimized for this problem
(specifically: having a faster method of neighborhood listing for undirected
graphs).

## License

See [here](../LICENSE.md) for details.