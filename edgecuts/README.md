# Edgecuts

This library contains the code to calculate Edgecut Weights (publication
pending; [author version](http://emallson.net/pubs/icwsm20_edgecuts.pdf)) from
offline network data. To do this, it contains an efficient implementation of
the [EBGStop][ebgstop].

## Use

To use this as a library in your own code, add the following to your `Cargo.toml`

```toml
[dependencies.edgecuts]
git = "https://gitlab.com/emallson/edgecuts.git
```

## Benchmarks

As part of this project, I evaluated several different Monte-Carlo methods for
estimating the mean of a random variable. Below are the benchmark results
(which can be run here with `cargo bench`):

 | Method                  | Source                         | Runtime (μs) | Mean Sample Count | Sample Std. Dev |
 | ----------------------- | ------------------------------ | ------------ | ----------------- | --------------- |
 | Stopping Rule Algorithm | [Dagum et. al. 2000][sra]      | 571.96       | 563.228           | 110.982         |
 | EBGStop                 | [Mnih et. al. 2008][ebgstop]   | **198.86**   | **155.224**       | **82.002**      |
 | Huber-Jones Stopping    | [Huber and Jones 2019][hjstop] | 563.67       | 557.955           | 410.79          |
 | GBAS                    | [Huber 2017][gbas]             | 478.09       | 445.418           | 88.354          |

## Citing

If you use this software, please cite the following paper: 

```bibtex
@paper{smith_edgecuts_2020,
    author = {J. David Smith and My T. Thai},
    title = {Measuring Edge Sparsity on Large Social Networks},
    conference = {International AAAI Conference on Web and Social Media},
    year = {2020},
    keywords = {},
}
```

## License

See [here](../LICENSE.md) for details.

[sra]: http://epubs.siam.org/doi/abs/10.1137/S0097539797315306
[ebgstop]: https://dl.acm.org/doi/abs/10.1145/1390156.1390241
[hjstop]: http://www.sciencedirect.com/science/article/pii/S037847541930031X
[gbas]: https://onlinelibrary.wiley.com/doi/abs/10.1002/rsa.20654