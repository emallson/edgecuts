graph_names = {'ca-GrQc': 'GrQc',
               'ca-AstroPh': 'AstroPh',
               'nethept': 'NetHept',
               'email-Enron': 'Enron Email',
               'ia-email-EU': 'EU Email',
               'email-EU-inout': 'EU Email',
               'soc-Slashdot0902': 'Slashdot',
               'com-dblp.ungraph': 'DBLP',
               'com-orkut.ungraph': 'Orkut',
               'btc-1k': 'BTC (1k)',
               'gg_users': 'GG Users',
               'gg_giant_component': 'GG Giant',
               'gg_valid': '\\#GamerGate',
               'ca-HepPh': 'HepPh',
               'musae_facebook': "Facebook Pages",
               'wiki-Talk': 'Wiki Talk',
               'twitter-big': 'Twitter (2010)',
               'ia-fb-messages': 'Facebook Messages',
               'btc': 'Bitcoin Transactions',
               'btc_addresses_noneg': 'Bitcoin Addresses',
               'rt-higgs': 'Higgs Retweets'}

labels = {'btw': 'Edge Betweenness',
          'edgecuts': 'Edgecut Weight',
          'riondato': 'Riondato et. al.'}

safe_btw_graphs = ['ca-GrQc', 'nethept', 'email-Enron', 'soc-Slashdot0902', 'com-dblp.ungraph']
