import sys
from .walker import setup_apis, setup_db
from .edgecuts import edgecut_weight
from .walker import neighbors, mhrw, neighbor_count
import csv
import argparse
import time
import numpy as np
from pymc3 import rhat
import itertools as it

parser = argparse.ArgumentParser(description="Calculate Edgecut Weights via the Twitter API")
parser.add_argument('sample_count', type=int)
parser.add_argument('--output', dest='output', action='store')
parser.add_argument('--epsilon', dest='epsilon', type=float, action='store', default=0.2)
parser.add_argument('--delta', dest='delta', type=float, action='store', default=0.1)
parser.add_argument('--rho', dest='rho', type=float, action='store', default=0.2)
parser.add_argument('--root', dest='roots', type=str, action='append')
parser.add_argument('--target-r', dest='r', type=float, action='store', default=1.02)
parser.add_argument('--step-size', dest='step_size', type=int, action='store', default=10)

args = parser.parse_args()

setup_db()
setup_apis()

writer = None
out = None
if args.output is not None:
    out = open(args.output, 'a')
    writer = csv.writer(out)
else:
    out = sys.stdout
    writer = csv.writer(sys.stdout)


walks = [mhrw(root) for root in args.roots]
degrees = [[] for root in args.roots]
while len(degrees[0]) < 10 or rhat(np.array(degrees)) > args.r:
    # burn-in period
    for walk, deg in zip(walks, degrees):
        v = next(walk)
        deg.append(len(neighbors(v)))
    print(rhat(np.array(degrees)))

walk_loop = it.cycle(walks)
num_sampled = 0
while num_sampled < args.sample_count:
    walk = next(walk_loop)
    source = None
    # we take larger steps between nodes to limit correlations between
    # repeated samples from the same walk
    for i in range(args.step_size):
        source = next(walk)
    targets = neighbors(source)
    target = np.random.choice(list(targets))
    if neighbor_count(target) > 300000:
        continue  # skip this target
    print('EW: {}---{}'.format(source, target), file=sys.stderr)

    weight, est = edgecut_weight(source, target, args.epsilon, args.delta, args.rho)
    writer.writerow([source, target, weight, est.mean, est.variance, est.count])
    out.flush()
