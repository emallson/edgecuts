use crate::brandes::sssp_unweighted_undirected;
use crate::brandes::{EdgeCount, FindEdge};
use petgraph::graph::GraphIndex;
use petgraph::visit::{
    GraphRef, IntoNeighbors, IntoNodeIdentifiers, NodeCompactIndexable, NodeCount,
};
use rand::distributions::WeightedIndex;
use rand::prelude::*;
use rayon::prelude::*;
use edgecuts::saw::RandomNodeIndexable;
use sfmt::thread_rng;
use slog::Logger;
use std::hash::Hash;
use std::sync::atomic::{AtomicUsize, Ordering};

#[derive(Debug)]
pub enum RiondatoError {
    /// Returned when the number of samples required exceeds the number of edges in the network, at
    /// which point just running the exact method will be faster.
    TooManySamples { n_samples: usize, n_edges: usize },
}

impl std::fmt::Display for RiondatoError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        <Self as std::fmt::Debug>::fmt(self, f)
    }
}

pub fn est_vertex_diameter<G, R>(g: G, rng: &mut R) -> usize
where
    R: Rng,
    G: GraphRef + IntoNeighbors + NodeCount + RandomNodeIndexable,
    G::NodeId: Copy + GraphIndex + Hash + Send + Eq,
{
    let source = g.rand_node(rng);
    let mut dists = vec![None; g.node_count()];
    let mut sigma = vec![0; g.node_count()];
    let mut pred = vec![vec![]; g.node_count()];
    // TODO: this can be *much* faster than doing a scan through dists after
    sssp_unweighted_undirected(g, source, None, &mut dists, &mut sigma, &mut pred);

    let mut first = 0;
    let mut second = 0;
    for dist in dists
        .into_iter()
        .filter(Option::is_some)
        .map(Option::unwrap)
    {
        if dist > first {
            second = first;
            first = dist;
        }
    }

    first + second
}

// this implementation violates the method described in Riondato & Kornaropoulos (2016) in that
// it effectively performs "rejection sampling" of empty samples (where u,v are not connected)
// by sampling v from the reachable set of u.
#[allow(clippy::many_single_char_names)] // copying variable names from the paper means a lot of single-variable names
pub fn approx_edge_betweenness<G>(
    g: G,
    eps: f64,
    delta: f64,
    p: f64,
    log: Logger,
) -> Result<Vec<f64>, RiondatoError>
where
    G: GraphRef
        + IntoNeighbors
        + IntoNodeIdentifiers
        + NodeCount
        + EdgeCount
        + FindEdge
        + NodeCompactIndexable
        + Sync,
    G::NodeId: Copy + GraphIndex + Hash + Eq + Send,
    G::EdgeId: Copy + GraphIndex,
{
    let mut rng = thread_rng();
    let mut diameter = 0.0;
    while diameter <= 2.0 {
        // will loop forever on trivial networks
        diameter = est_vertex_diameter(g, &mut rng) as f64;
    }

    let c = 0.5; // see Thm 1
    let r = (c / (p * eps.powi(2))
        * (((diameter - 1.0).log2().floor() + 1.0) * (-p.ln()) - delta.ln()))
    .ceil() as usize; // see Sec 6.3

    // it is possible that riondato can be faster when r > n, since we don't have the same
    // stack handling. however, when r > m it definitely won't be as the complexity at that
    // point is m^2. Even in the case where r > n, it seems likely to be within a factor of 2-3
    // times as fast as brandes
    //
    // might be possible to tighten this to 2n
    if r > g.edge_count() {
        return Err(RiondatoError::TooManySamples {
            n_samples: r,
            n_edges: g.edge_count(),
        });
    }

    info!(
        log,
        "approximating betweenness centrality with {r} samples",
        r = r;
        "diameter" => diameter,
    );

    let mut b = Vec::with_capacity(g.edge_count());
    for _ in 0..g.edge_count() {
        b.push(AtomicUsize::new(0));
    }

    let sigma = vec![0; g.node_count()];
    let dists = vec![None; g.node_count()];
    let pred = vec![vec![]; g.node_count()];
    let empty_samples: usize = (0..r)
        .into_par_iter()
        .map_with(
            (sigma, dists, pred),
            |(ref mut sigma, ref mut dists, ref mut pred), _i| {
                let mut rng = thread_rng();
                let u = g.rand_node(&mut rng);

                let stack = sssp_unweighted_undirected(g, u, None, dists, sigma, pred);

                if stack.len() == 1 {
                    // only reached u itself
                    return 1;
                }

                // breaking the rules a bit, lets do some pseudo-rejection sampling
                let v = *stack.choose(&mut rng).unwrap();

                let mut t = v;
                while t != u {
                    let p = &pred[t.index()];
                    let weights = p.iter().map(|y| sigma[y.index()]).collect::<Vec<_>>();

                    let dist = WeightedIndex::new(weights).unwrap();
                    let x = p[dist.sample(&mut rng)];
                    let e = g.find_edge(x, t).unwrap();
                    b[e.index()].fetch_add(1, Ordering::Relaxed);
                    t = x;
                }

                0
            },
        )
        .sum();

    // because I cheated and sampled v from the reachable set, the only way we can get an empty
    // sample is if we pick an isolated node.
    if empty_samples > 0 {
        warn!(log, "{samples} empty samples!", samples = empty_samples);
    }

    let n = g.node_count() as f64;
    let scale = 1.0 / (n * (n - 1.0));
    Ok(b.into_par_iter()
        .map(|w| w.load(Ordering::Relaxed) as f64 / r as f64 * scale)
        .collect())
}
