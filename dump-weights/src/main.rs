#[macro_use]
extern crate slog;
extern crate structopt;

use graph_parse::edgelist::load;
use petgraph::visit::{IntoEdgeReferences, NodeCount, EdgeRef};
use petgraph::Undirected;
use vec_graph::{Graph};
use std::fs::File;
use structopt::StructOpt;
use slog::{Drain, Logger};

#[derive(StructOpt, Debug, Copy, Clone)]
#[structopt(rename_all = "kebab-case")]
pub enum WeightMethod {
    Uniform,
    ExactBetweenness,
    DistBetweenness {
        #[structopt(default_value = "5")]
        dist: usize,
    },
    ApproxBetweenness {
        #[structopt(default_value = "0.1")]
        eps: f64,
        #[structopt(default_value = "0.1")]
        delta: f64,
        #[structopt(default_value = "0.01")]
        p: f64,
    },
    EdgecutApprox {
        /// Walk stop probability
        stop_prob: f64,
        /// approximation factor
        eps: f64,
        /// probability of approximation holding
        delta: f64,
    },
}

#[derive(StructOpt, Debug)]
#[structopt(
    name = "dump",
    about = "Dump the generated weights for a network to a file for external analysis."
)]
struct Args {
    graph: String,
    output: String,
    #[structopt(subcommand)]
    method: WeightMethod,
    #[structopt(long)]
    log: Option<String>,
    #[structopt(long)]
    threads: Option<usize>,
    #[structopt(long)]
    disable_dedup: bool,
}

pub fn build_logger(path: Option<String>) -> Result<Logger, ::std::io::Error> {
    use slog::Duplicate;
    use slog_async::Async;
    use slog_json::Json;

    let term = Async::new(slog_term::term_compact().fuse()).build().fuse();

    if let Some(path) = path {
        let file = File::create(path)?;
        let file_drain = Async::new(Json::default(file).fuse()).build().fuse();
        Ok(Logger::root(Duplicate::new(file_drain, term).fuse(), o!()))
    } else {
        Ok(Logger::root(term, o!()))
    }
}

pub fn reweight<N: Sync>(g: &mut Graph<N, f64, Undirected>, method: WeightMethod, log: Logger) {
    match method {
        WeightMethod::Uniform => g.edge_weights_mut().for_each(|w| *w = 1.0),
        WeightMethod::EdgecutApprox {
            stop_prob,
            eps,
            delta,
        } => {
            let weights = edgecuts::edgecut_weights(g, stop_prob, eps, delta);
            g.edge_weights_mut().zip(weights).for_each(|(w, p)| *w = p);
        },
        WeightMethod::ExactBetweenness => {
            let cs = edge_betweenness::brandes::edge_unweighted_undirected(&*g, None, None);
            g.edge_weights_mut().zip(cs).for_each(|(w, p)| *w = p);
        }
        WeightMethod::DistBetweenness { dist } => {
            let cs = edge_betweenness::brandes::edge_unweighted_undirected(&*g, Some(dist), None);
            g.edge_weights_mut().zip(cs).for_each(|(w, p)| *w = p);
        }
        WeightMethod::ApproxBetweenness { eps, delta, p } => {
            let cs = edge_betweenness::riondato::approx_edge_betweenness(
                &*g,
                eps,
                delta,
                p,
                log.new(o!("section" => "riondato")),
            );
            if let Ok(cs) = cs {
                g.edge_weights_mut().zip(cs).for_each(|(w, p)| *w = p);
            } else {
                use edge_betweenness::riondato::RiondatoError::*;
                match cs.unwrap_err() {
                    TooManySamples { n_samples, n_edges } => {
                        error!(log, "riondato method requires too many samples"; "n_samples" => n_samples, "n_edges" => n_edges);
                    }
                }
            }
        }
    }
}

pub fn dump_weights<N: std::fmt::Display, Ty: petgraph::EdgeType>(
    path: &str,
    g: &Graph<N, f64, Ty>,
) -> Result<(), std::io::Error> {
    use std::io::{BufWriter, Write};
    let mut out = BufWriter::new(File::create(path)?);

    for edge in g.edge_references() {
        writeln!(
            out,
            "{} {} {}",
            g[edge.source()],
            g[edge.target()],
            edge.weight()
        )?;
    }

    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::from_args();

    println!("{:?}", args);
    let log = build_logger(args.log)?;

    if let Some(t) = args.threads {
        rayon::ThreadPoolBuilder::new()
            .num_threads(t)
            .build_global()
            .unwrap();
    }

    let mut file = File::open(&args.graph)?;
    let mut g = if args.graph.ends_with(".gz") {
        use flate2::read::GzDecoder;
        load(&mut GzDecoder::new(file), true)?
    } else {
        load(&mut file, true)?
    };

    info!(log, "loaded graph from {path}", path=&args.graph; "n" => g.node_count(), "m" => g.edge_count());
    reweight(&mut g, args.method, log.new(o!("section" => "reweight")));
    info!(log, "reweighted graph");

    dump_weights(&args.output, &g)?;
    info!(log, "dumped weights");

    Ok(())
}
