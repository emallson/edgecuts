use petgraph::graph::Graph;
use petgraph::visit::{EdgeRef, GraphRef, IntoEdges, IntoNodeIdentifiers, NodeCompactIndexable};
use petgraph::Undirected;
use rand::distributions::{Distribution, Uniform};
use rand::seq::IteratorRandom;
use std::collections::BTreeSet;

pub trait RandomNodeIndexable: GraphRef {
    fn rand_node<R: rand::Rng>(self, rng: &mut R) -> Self::NodeId;
}

pub trait RandomNeighbor: GraphRef {
    fn rand_neighbor<R: rand::Rng>(
        self,
        node: Self::NodeId,
        rng: &mut R,
    ) -> Option<(Self::EdgeId, Self::NodeId)>;
}

#[derive(Debug)]
pub struct Walk<G: GraphRef> {
    pub edges: Vec<(G::EdgeId, G::NodeId)>,
    pub start: G::NodeId,
    pub end: G::NodeId,
}

// may have to switch to nightly for specialization so i can fall back for anything that isn't
// compactly indexable, but right now vecgraph is and thats what i'm building on
impl<G: GraphRef + NodeCompactIndexable> RandomNodeIndexable for G {
    fn rand_node<R: rand::Rng>(self, rng: &mut R) -> G::NodeId {
        self.from_index(rng.gen_range(0, self.node_count()))
    }
}

impl<N, E> RandomNeighbor for &Graph<N, E, Undirected> {
    fn rand_neighbor<R: rand::Rng>(
        self,
        node: Self::NodeId,
        rng: &mut R,
    ) -> Option<(Self::EdgeId, Self::NodeId)> {
        let eref = self.edges(node).choose(rng);
        eref.map(|eref| (eref.id(), eref.target()))
    }
}

use petgraph::Direction::{Incoming, Outgoing};
use rand::seq::SliceRandom;
use vec_graph::VecGraph;

impl<N, E> RandomNeighbor for &VecGraph<N, E, Undirected> {
    
    #[allow(clippy::len_zero)]
    fn rand_neighbor<R: rand::Rng>(
        self,
        node: Self::NodeId,
        rng: &mut R,
    ) -> Option<(Self::EdgeId, Self::NodeId)> {
        use rand::distributions::Bernoulli;
        let inc = self.raw_edges(node, Incoming);
        let out = self.raw_edges(node, Outgoing);

        // fast path for trivial cases
        if inc.len() + out.len() == 0 {
            return None;
        } else if inc.len() == 1 && out.len() == 0 {
            let ix = inc[0];
            let (tar, _, _) = self.raw_edgelist()[ix];
            return Some((Self::EdgeId::new(ix), Self::NodeId::new(tar)));
        } else if out.len() == 1 && inc.len() == 0 {
            let ix = out[0];
            let (_, tar, _) = self.raw_edgelist()[ix];
            return Some((Self::EdgeId::new(ix), Self::NodeId::new(tar)));
        }

        let d = Bernoulli::new(inc.len() as f64 / (inc.len() as f64 + out.len() as f64));
        if d.sample(rng) {
            let ix = inc.choose(rng)?;
            let (tar, _, _) = self.raw_edgelist()[*ix];
            Some((Self::EdgeId::new(*ix), Self::NodeId::new(tar)))
        } else {
            let ix = out.choose(rng)?;
            let (_, tar, _) = self.raw_edgelist()[*ix];
            Some((Self::EdgeId::new(*ix), Self::NodeId::new(tar)))
        }
    }
}

pub fn self_avoiding_walk<
    G: GraphRef + IntoEdges + IntoNodeIdentifiers + RandomNodeIndexable,
    R: rand::Rng,
>(
    g: G,
    rng: &mut R,
    start: Option<G::NodeId>,
    stop_prob: f64,
) -> Walk<G>
where
    G::NodeId: Ord,
{
    let start = start.unwrap_or_else(|| g.rand_node(rng));
    let dist = Uniform::new(0.0, 1.0);

    // allocate enough space to hold a walk of expected size
    // e.g. stop_prob = 0.1 gives us 10 edges worth of space
    let mut walk = Vec::with_capacity((1.0 / stop_prob).ceil() as usize);
    let mut visited = BTreeSet::new();
    visited.insert(start);

    let mut cur = start;
    while dist.sample(rng) > stop_prob {
        if let Some(edge) = g
            .edges(cur)
            .filter(|c| !visited.contains(&c.target()))
            .choose(rng)
        {
            cur = edge.target();
            visited.insert(cur);
            walk.push((edge.id(), edge.target()));
        } else {
            // no more nodes
            break;
        }
    }
    Walk {
        start,
        end: cur,
        edges: walk,
    }
}

pub fn random_walk<
    G: GraphRef + IntoEdges + IntoNodeIdentifiers + RandomNodeIndexable,
    R: rand::Rng,
>(
    g: G,
    rng: &mut R,
    start: Option<G::NodeId>,
    stop_prob: f64,
) -> Walk<G>
where
    G::NodeId: Ord,
{
    let start = start.unwrap_or_else(|| g.rand_node(rng));
    let dist = Uniform::new(0.0, 1.0);

    // allocate enough space to hold a walk of expected size
    // e.g. stop_prob = 0.1 gives us 10 edges worth of space
    let mut walk = Vec::with_capacity((1.0 / stop_prob).ceil() as usize);

    let mut cur = start;
    while dist.sample(rng) > stop_prob {
        if let Some(edge) = g.edges(cur).choose(rng) {
            cur = edge.target();
            walk.push((edge.id(), edge.target()));
        } else {
            // no more nodes
            break;
        }
    }

    Walk {
        start,
        end: cur,
        edges: walk,
    }
}

pub fn edgecut_walk<
    G: GraphRef + IntoEdges + IntoNodeIdentifiers + RandomNodeIndexable + RandomNeighbor,
    R: rand::Rng,
>(
    g: G,
    cut: G::EdgeId,
    rng: &mut R,
    start: Option<G::NodeId>,
    stop_prob: f64,
) -> Walk<G>
where
    G::NodeId: Ord,
{
    let start = start.unwrap_or_else(|| g.rand_node(rng));
    let dist = Uniform::new(0.0, 1.0);

    // allocate enough space to hold a walk of expected size
    // e.g. stop_prob = 0.1 gives us 10 edges worth of space
    let mut walk = Vec::with_capacity((1.0 / stop_prob).ceil() as usize);

    let mut cur = start;
    'outer: while dist.sample(rng) > stop_prob {
        if let Some((edge, target)) = g.rand_neighbor(cur, rng) {
            if edge == cut && g.edges(cur).all(|e| e.id() == cut) {
                // no other edges to go to, but the selected edge has been cut so we can't
                // go anywhere
                break 'outer;
            }
            cur = target;
            walk.push((edge, target));
        } else {
            // no more nodes
            break 'outer;
        }
    }

    Walk {
        start,
        end: cur,
        edges: walk,
    }
}
