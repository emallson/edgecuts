#[macro_use]
extern crate criterion;

use criterion::{black_box, BatchSize::SmallInput, Criterion};
use edgecuts::alternate_estimators::*;
use edgecuts::{ebgstop, edgecut_sampler};
use graph_parse::edgelist::load;
use online_moments::MomentEstimator;
use petgraph::Undirected;
use rand::distributions::{Distribution, Uniform};
use sfmt::thread_rng;
use std::fs::File;
use std::sync::{Arc, Mutex};
use vec_graph::{EdgeIndex, Graph};

const GRAPH: &str = "../data/mono/com-dblp.ungraph.txt";
const EPS: f64 = 0.2;
const DELTA: f64 = 0.01;
const STOP_PROB: f64 = 0.2;

fn random_edge<N, E>(g: &Graph<N, E, Undirected>) -> EdgeIndex {
    let mut rng = thread_rng();
    let dist = Uniform::new(0, g.edge_count());
    EdgeIndex::new(dist.sample(&mut rng))
}

macro_rules! bench_sampler {
    ($name:ident, $sampler:ident, $($e:expr),*) => (
        fn $name(c: &mut Criterion) {
    let g = load(&mut File::open(GRAPH).unwrap(), true).unwrap();
    let est = Arc::new(Mutex::new(MomentEstimator::new(2)));

    {
        let est = est.clone();
        c.bench_function(stringify!($sampler), move |b| {
            b.iter_batched(
                || random_edge(&g),
                |e| {
                    let res = $sampler(edgecut_sampler(&g, STOP_PROB, e), EPS, DELTA $(, $e)*);
                    est.lock().unwrap().update(black_box(res.n_samples as f64))
                },
                SmallInput,
            );
        });
    }

    let est = est.lock().unwrap();
    println!(
        "Sample Dist Info: mean = {}, variance = {}",
        est.mean(),
        est.variance().unwrap(),
    );
        }
    );
    ($name:ident, $sampler:ident) => (
        bench_sampler!($name, $sampler,);
    )
}

bench_sampler!(bench_sra, stopping_rule_alg);
bench_sampler!(bench_ebgstop, ebgstop);
bench_sampler!(bench_hjstop, huber_jones_stopping, &mut thread_rng());
bench_sampler!(bench_gbas, gbas, &mut thread_rng());

criterion_group!(algs, bench_sra, bench_ebgstop, bench_hjstop, bench_gbas);
criterion_main!(algs);
