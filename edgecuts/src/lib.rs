use online_moments::MomentEstimator;
use petgraph::Undirected;
use rayon::prelude::*;
use sfmt::thread_rng;
use std::collections::HashSet;
use vec_graph::{EdgeIndex, Graph};

#[cfg(feature = "progress")]
use std::sync::channel;

pub mod saw;
pub mod alternate_estimators;

#[derive(Debug)]
pub struct SamplingResult {
    pub estimate: f64,
    pub sample_dist: Option<MomentEstimator>,
    pub n_samples: usize,
}

/// Implements the EBGStop algorithm of Mnih et. al. (2008)
///
/// This produces a 1 - ε approximation of the mean of Z based on samples Z_i taken from
/// `sample_fn` which holds with probability 1 - δ.
///
/// However, when the true mean is exactly 0, this method **will not terminate!** (And as it
/// approaches 0, it will take proportionally longer to run)
pub fn ebgstop<F>(mut sample_fn: F, eps: f64, delta: f64) -> SamplingResult
where
    F: FnMut() -> f64,
{
    assert!(0.0 < eps && eps < 1.0 && 0.0 < delta && delta < 1.0);

    #[allow(non_upper_case_globals)]
    const beta: f64 = 1.1;
    #[allow(non_upper_case_globals)]
    const p: f64 = 1.1;

    let c = delta * (1.0 - p.recip());

    let mut lb: f64 = 0.0;
    let mut ub = std::f64::INFINITY;
    let mut t = 1.0;
    let mut k = 0;

    let mut est = MomentEstimator::new(2);

    let mut alpha;
    let mut x = std::f64::NAN;

    while (1.0 + eps) * lb < (1.0 - eps) * ub {
        t += 1.0;
        let xt = sample_fn();
        est.update(xt);

        let bk = beta.powi(k).floor();
        if t > bk {
            k += 1;
            alpha = beta.powi(k).floor() / bk;
            let dk = c / t.log(beta).powf(p);
            x = -alpha * dk.ln() / 3.0;
        }
        let x_mean = est.mean();
        let sigma_t = est.variance().unwrap().sqrt();
        // R is taken as 1.0
        let ct = sigma_t * (2.0 * x / t).sqrt() + 3.0 * x / t;
        lb = lb.max(x_mean - ct);
        ub = ub.min(x_mean + ct);
    }

    // only support X_i \in [0, 1] so sgn(X_t) = 1
    SamplingResult {
        estimate: ((1.0 + eps) * lb + (1.0 - eps) * ub) / 2.0,
        sample_dist: Some(est),
        n_samples: t as usize,
    }
}

// Produces a function suitable for repeatedly sampling Z_i for an individual edge `idx`. 
pub fn edgecut_sampler<'a, N, E>(
    g: &'a Graph<N, E, Undirected>,
    stop_prob: f64,
    idx: EdgeIndex,
) -> impl FnMut() -> f64 + 'a {
    use saw::edgecut_walk;
    let (left, right) = g.edge_endpoints(idx).unwrap();
    move || {
        let walk = edgecut_walk(g, idx, &mut thread_rng(), Some(left), stop_prob);

        let left_nodes = walk
            .edges
            .into_iter()
            .map(|(_, node)| node)
            .collect::<HashSet<_>>();

        let walk = edgecut_walk(g, idx, &mut thread_rng(), Some(right), stop_prob);
        if walk
            .edges
            .into_iter()
            .any(|(_, node)| left_nodes.contains(&node))
        {
            0.0
        } else {
            1.0
        }
    }
}

/// Calculate a single Edgecut Weight using the EBGStop algorithm. This procedure returns an
/// estimate with error margin 1 ± ϵ that holds with probability at least 1 - δ.
pub fn edgecut_weight<'a, N, E>(
    g: &'a Graph<N, E, Undirected>,
    stop_prob: f64,
    idx: EdgeIndex,
    epsilon: f64,
    delta: f64,
) -> f64 {
    ebgstop(edgecut_sampler(g, stop_prob, idx), epsilon, delta).estimate
}

/// Calculates the edgecut weights of each edge in `g` using `ebgstop`.
#[allow(clippy::let_unit_value)]
pub fn edgecut_weights<'a, N: Sync, E: Sync>(
    g: &'a Graph<N, E, Undirected>,
    stop_prob: f64,
    eps: f64,
    delta: f64,
) -> Vec<f64> {
    let mut weights = vec![0.0; g.edge_count()];

    #[cfg(feature = "progress")]
    let (send, recv) = channel();

    #[cfg(not(feature = "progress"))]
    let send = ();

    let edges: Vec<_> = g.edge_indices().collect();

    #[cfg(feature = "progress")]
    let total = edges.len() as u64;
    #[cfg(feature = "progress")]
    let threshold = edges.len() as u64 / 100;

    #[cfg(feature = "progress")]
    let handle = spawn(move || {
        use indicatif::{ProgressBar, ProgressStyle};
        let bar = ProgressBar::new(total);
        bar.set_message("sampling edge weights");
        bar.set_style(
            ProgressStyle::default_bar().template("{msg} {wide_bar} {pos}/{len} ETA: {eta}"),
        );
        bar.tick();
        let mut count = 0;
        for _ in recv.iter() {
            count += 1;
            if count > threshold {
                bar.inc(count);
                count = 0;
            }
        }

        bar.finish();
    });

    #[allow(unused_variables)] // send is unused unless progress is enabled
    edges
        .into_par_iter()
        .map_with((g, send), |(g, send), idx| {
            #[cfg(feature = "progress")]
            _send.send(()).unwrap();
            edgecut_weight(*g, stop_prob, idx, eps, delta)
        })
        .collect_into_vec(&mut weights);

    #[cfg(feature = "progress")]
    handle.join().unwrap();

    weights
}

#[cfg(test)]
mod test {
    const EPS: f64 = 0.01;

    // this is not a test of correctness, but consistency. The weights here are calculated using
    // this test. We are only checking that they don't change as a product of other changes.
    //
    // This test should fail no more than once in every 10,000 runs.
    #[test]
    fn first_ten_edge() {
        use graph_parse::edgelist::load;
        use std::fs::File;

        let grqc = File::open("../data/mono/ca-GrQc.txt")
            .map(|f| load(f, false).unwrap())
            .unwrap();
        let indices = grqc.edge_indices().take(10);

        const TRUE_WEIGHTS: [f64; 10] = [
            0.705, 0.665, 0.697, 0.869, 0.762, 0.886, 0.600, 0.609, 0.873, 0.872,
        ];
        // we don't use a parallel loop here because `cargo test` runs things in parallel and we
        // don't want to get in the way
        for (idx, &tw) in indices.zip(&TRUE_WEIGHTS) {
            let weight = super::edgecut_weight(&grqc, 0.1, idx, EPS, 1e-6);
            println!("{} {}", tw, weight);
            assert!(weight >= (1.0 - EPS) * tw && weight <= (1.0 + EPS) * tw);
        }
    }
}
