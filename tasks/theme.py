import matplotlib.pyplot as plt
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{times} \usepackage{helvet}')

import plotnine
full_theme = plotnine.themes.theme_seaborn(context='paper', style='ticks') +\
    plotnine.themes.theme(
        dpi=100, 
        plot_margin=0, 
        figure_size=(3.4,1.5), 
        text=plotnine.element_text(size=10), 
        legend_title=plotnine.element_blank(), 
        plot_title=plotnine.element_blank(), 
        legend_position=(.8, .60),
    )

half_theme = full_theme + plotnine.themes.theme(
    figure_size=(1.6, 1.5),
    legend_position=(.6, .6),
)
