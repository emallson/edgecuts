use hashbrown::HashMap;
use petgraph::graph::GraphIndex;
use petgraph::prelude::*;
use petgraph::visit::{
    GraphBase, GraphRef, IntoNeighbors, IntoNodeIdentifiers, NodeCount,
};
use petgraph::EdgeType;
use rayon::prelude::*;
use std::collections::VecDeque;
use std::hash::Hash;
use std::sync::{Arc, Mutex};
use vec_graph::VecGraph;

pub trait EdgeCount: GraphBase {
    fn edge_count(&self) -> usize;
}

impl<V, E, Ty: EdgeType> EdgeCount for &Graph<V, E, Ty> {
    fn edge_count(&self) -> usize {
        Graph::edge_count(self)
    }
}

impl<V, E, Ty: EdgeType> EdgeCount for &VecGraph<V, E, Ty> {
    fn edge_count(&self) -> usize {
        VecGraph::edge_count(self)
    }
}

pub trait FindEdge: GraphBase {
    fn find_edge(&self, u: Self::NodeId, v: Self::NodeId) -> Option<Self::EdgeId>;
}

impl<V, E, Ty: EdgeType> FindEdge for &Graph<V, E, Ty> {
    fn find_edge(&self, u: Self::NodeId, v: Self::NodeId) -> Option<Self::EdgeId> {
        Graph::find_edge(self, u, v)
    }
}

impl<V, E, Ty: EdgeType> FindEdge for &VecGraph<V, E, Ty> {
    fn find_edge(&self, u: Self::NodeId, v: Self::NodeId) -> Option<Self::EdgeId> {
        VecGraph::find_edge(self, u, v)
    }
}

/// Calculate the SSSP from a node `s` to all other nodes within `max_dist` of `s`. The `dist`
/// vector is re-used between iterations. This function initializes it appropriately.
pub(crate) fn sssp_unweighted_undirected<G>(
    g: G,
    s: G::NodeId,
    max_dist: Option<usize>,
    dist: &mut Vec<Option<usize>>,
    sigma: &mut Vec<usize>,
    pred: &mut Vec<Vec<G::NodeId>>,
) -> Vec<G::NodeId>
where
    G: GraphRef + IntoNeighbors + NodeCount,
    G::NodeId: Copy + GraphIndex + Hash + Send + Eq,
{
    for i in 0..g.node_count() {
        sigma[i] = 0;
        dist[i] = None;
        pred[i].clear();
    }
    sigma[s.index()] = 1;
    dist[s.index()] = Some(0);

    let mut stack = Vec::new();
    let mut queue = VecDeque::new();
    queue.push_back(s);

    while let Some(v) = queue.pop_front() {
        stack.push(v);

        let dv = dist[v.index()].unwrap();
        for w in g.neighbors(v) {
            if dist[w.index()].is_none() {
                dist[w.index()] = Some(dv + 1);
                if max_dist.map(|max| dv < max).unwrap_or(true) {
                    queue.push_back(w);
                }
            }
            if let Some(dw) = dist[w.index()] {
                if dw == dv + 1 {
                    sigma[w.index()] += sigma[v.index()];
                    pred[w.index()].push(v);
                }
            }
        }
    }

    stack
}

/// Calculate the betweenness centrality of each edge in a network via Brandes (2001).
pub fn edge_unweighted_undirected<G>(
    g: G,
    max_dist: Option<usize>,
    nodes: Option<Vec<G::NodeId>>,
) -> Vec<f64>
where
    G: GraphRef + IntoNeighbors + IntoNodeIdentifiers + NodeCount + EdgeCount + FindEdge + Sync,
    G::NodeId: Copy + GraphIndex + Hash + Eq + Send,
    G::EdgeId: Copy + GraphIndex,
{
    let centrality = Arc::new(Mutex::new(vec![0.0; g.edge_count()]));
    let sigma = vec![0; g.node_count()];
    let dist = vec![None; g.node_count()];
    let pred = vec![vec![]; g.node_count()];
    let nodes = match nodes {
        Some(v) => v,
        None => g.node_identifiers().collect::<Vec<_>>(),
    };
    nodes.into_par_iter().for_each_with(
        (sigma, dist, pred),
        |(ref mut sigma, ref mut dist, ref mut pred), s| {
            let mut stack = sssp_unweighted_undirected(g, s, max_dist, dist, sigma, pred);

            let mut delta = HashMap::new();

            // based heavily on the networkx implementation of accumulating edge weights
            while let Some(w) = stack.pop() {
                let coeff = (1.0 + *delta.entry(w).or_insert(0.0) as f64) / sigma[w.index()] as f64;
                let preds = &pred[w.index()];
                assert!(
                    !preds.is_empty() || s == w,
                    "missing predecessors at pos {}",
                    stack.len()
                );
                for &v in preds {
                    let c = sigma[v.index()] as f64 * coeff;
                    let idx = g.find_edge(v, w).unwrap();
                    let mut cent = centrality.lock().unwrap();
                    cent[idx.index()] += c;
                    *delta.entry(v).or_insert(0.0) += c;
                }
            }
        },
    );

    // normalize the weights
    let n = g.node_count() as f64;
    let centrality = Arc::try_unwrap(centrality).unwrap();
    let mut centrality = centrality.into_inner().unwrap();
    for c in &mut centrality {
        *c /= n * (n - 1.0);
    }

    centrality
}

#[cfg(test)]
mod test {
    use super::*;
    use graph_parse::edgelist::load;
    use petgraph::visit::NodeIndexable;
    use std::fs::File;

    #[test]
    fn reverse_connectivity() {
        let mut file = File::open("../data/mono/ca-GrQc.txt").unwrap();
        let g = load(&mut file, true).unwrap();

        for u in g.node_indices() {
            for v in g.neighbors(u) {
                assert!(g.neighbors(v).any(|x| x == u));
            }
        }
    }

    #[test]
    fn sssp_without_dups() {
        let mut file = File::open("../data/mono/ca-GrQc.txt").unwrap();
        let g = load(&mut file, true).unwrap();

        let mut dist = vec![None; g.node_count()];
        let mut sigma = vec![0; g.node_count()];
        let mut pred = vec![vec![]; g.node_count()];
        let stack = sssp_unweighted_undirected(
            &g,
            g.from_index(1),
            Some(2),
            &mut dist,
            &mut sigma,
            &mut pred,
        );

        for u in &stack[1..] {
            println!("{:?} {:?} {}", u, dist[u.index()], sigma[u.index()]);
            assert!(!pred[u.index()].is_empty());
        }
    }

    macro_rules! test_igraph {
        ($name:ident, $graph_path:expr) => {
            mod $name {
                use super::*;
                use std::io::Read;
                #[test]
                fn matches_igraph() {
                    let mut file = File::open(format!("../data/mono/{}.txt", $graph_path)).unwrap();
                    let g = load(&mut file, true).unwrap();
                    let weights = edge_unweighted_undirected(&g, None, None);

                    let mut weights_file =
                        File::open(format!("test-data/{}.txt", $graph_path)).unwrap();
                    let mut buf = String::new();
                    weights_file.read_to_string(&mut buf).unwrap();

                    let true_weights = buf
                        .split_whitespace()
                        .map(|w| w.parse::<f64>().unwrap())
                        .collect::<Vec<_>>();

                    assert!(weights
                        .into_iter()
                        .zip(true_weights.into_iter())
                        .map(|(w, t)| (w - t).abs())
                        .all(|diff| diff < 1e-6));
                }
            }
        };
    }

    test_igraph!(grqc, "ca-GrQc");
    test_igraph!(nethept, "nethept");
}
