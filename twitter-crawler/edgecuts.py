class WelfordVarianceEstimator:
    def __init__(self):
        self._count = 0
        self._mean = 0.0
        self._m2 = 0.0
        
    def update(self, sample):
        self._count += 1
        delta = sample - self._mean
        self._mean += delta / self._count
        delta2 = sample - self._mean
        self._m2 += delta * delta2
        
    @property
    def mean(self):
        return self._mean
    
    @property
    def variance(self):
        return self._m2 / self._count
    
    @property
    def sample_variance(self):
        return self._m2 / (self._count - 1)
    
    @property
    def count(self):
        return self._count


def ebgstop(sampler, eps, delta):
    from math import floor, log, sqrt
    BETA = 1.1
    P = 1.1
    
    c = delta * (1 - 1/P)
    lb = 0
    ub = float('inf')
    t = 1
    k = 0
    
    est = WelfordVarianceEstimator()
    
    alpha = None
    x = float('nan')
    
    while (1.0 + eps) * lb < (1.0 - eps) * ub:
        t += 1
        xt = sampler()
        est.update(xt)
        
        bk = floor(BETA ** k)
        if t > bk:
            k += 1
            alpha = floor(BETA ** k) / bk
            dk = c / (log(t, BETA) ** P)
            x = -alpha * log(dk) / 3 # log(x) is ln(x) when no base is specified
        x_mean = est.mean
        sigma_t = sqrt(est.variance)
        ct = sigma_t * sqrt(2 * x / t) + 3 * x / t
        lb = max(lb, x_mean - ct)
        ub = min(ub, x_mean + ct)
        
    return ((1 + eps) * lb + (1 - eps) * ub) / 2, est


def edgecut_weight(source, target, eps, delta, stop_prob=0.2):
    from .walker import edgecut_walk
    def sample():
        a = edgecut_walk(source, (source, target), stop_prob)
        b = edgecut_walk(target, (source, target), stop_prob)
        if set(a).isdisjoint(set(b)):
            return 1
        else:
            return 0
    return ebgstop(sample, eps, delta)
