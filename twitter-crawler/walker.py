import logging
import twitter
import lmdb
import json
import random
import time
import os
import sys
import requests
import numpy as np

logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d:%H:%M:%S',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

KEYS = None
APIS = None

def load_keys(path=None):
    global KEYS
    if path is None:
        path = os.path.join(os.path.dirname(__file__), 'keys.json')
    with open(path) as f:
        KEYS = json.load(f)


def setup_apis():
    global KEYS
    global APIS
    load_keys()
    APIS = [twitter.Api(sleep_on_rate_limit=False, **keys) for keys in KEYS]
    for api, keys in zip(APIS, KEYS):
        api._keyobj = keys
        api._erroring = False


env = None
FOLLOWERS = None
FRIENDS = None

def setup_db():
    global env
    global FOLLOWERS
    global FRIENDS
    path = os.path.join(os.path.dirname(__file__), "connections.lmdb")
    env = lmdb.open(path, max_dbs=5, map_size=int(1e12))

    FOLLOWERS = env.open_db('followers'.encode('utf8'))
    FRIENDS = env.open_db('friends'.encode('utf8'))


def free_api(uri):
    min_reset = float("inf")
    reset_api = None
    for api in APIS:
        limit = api.CheckRateLimit(uri)
        #  logger.info('Key Limit: %s %s', api._consumer_key, limit)
        if (not api._erroring and limit.remaining > 0) or limit.reset < time.time():
            if api._erroring:
                api._erroring = False
                api.SetCredentials(**api._keyobj)
            return api
        elif limit.reset < min_reset:
            min_reset = limit.reset
            reset_api = api
    # failed to find one, sleep until one is free
    sec = max(int(min_reset - time.time()) + 1, 0)
    logger.info("Sleeping for %d", sec)
    time.sleep(sec)
    return reset_api


def multi_iter(fn, url):
    result = []
    next_cursor = -1
    while next_cursor != 0:
        api = free_api(url)
        try:
            next_cursor, prev_cursor, contents = fn(api, next_cursor)
            result += contents
        except twitter.TwitterError as e:
            logger.warning('Twitter Error: %s. Stopping multi_iter', e)
            break
        except requests.exceptions.ConnectionError as error:
            logger.error('Connection Error {}, resetting APIs'.format(error))
            setup_apis()
    return result


def followers(user_id):
    with env.begin(db=FOLLOWERS) as txn:
        result = txn.get(user_id.encode('utf8'), None)
    if result is None:
        result = multi_iter(lambda api, c: api.GetFollowerIDsPaged(user_id, cursor=c, stringify_ids=True), '/followers/ids')
        with env.begin(write=True, db=FOLLOWERS) as txn:
            txn.put(user_id.encode('utf8'), json.dumps(result).encode('utf8'))
    else:
        result = json.loads(result)
    return result


def friends(user_id):
    with env.begin(db=FRIENDS) as txn:
        result = txn.get(user_id.encode('utf8'), None)
    if result is None:
        result = multi_iter(lambda api, c: api.GetFriendIDsPaged(user_id, cursor=c, stringify_ids=True), '/friends/ids')
        with env.begin(write=True, db=FRIENDS) as txn:
            txn.put(user_id.encode('utf8'), json.dumps(result).encode('utf8'))
    else:
        result = json.loads(result)
    return result


def neighbors(user_id):
    return set(friends(user_id)) | set(followers(user_id))


def edges(user_id):
    return [(user_id, n) for n in neighbors(user_id)]


def randbool(heads_prob):
    return random.uniform(0, 1) <= heads_prob


def lookup_users(user_ids):
    try:
        return free_api('/users/lookup').UsersLookup(user_id=user_ids)
    except twitter.TwitterError as e:
        logger.warning('Twitter Error: %s', e)
        return []


def neighbor_count(user_id):
    try:
        user = free_api("/users/show").GetUser(user_id=user_id)
    except twitter.TwitterError as e:
        logger.warning('Twitter Error: %s', e)
        return 0
    if user.protected:
        return 0 # can't touch them anyway

    return user.followers_count + user.friends_count

def edgecut_walk(root, cut, stop_prob=0.2):
    nodes = [root]
    current = root
    while randbool(1 - stop_prob):
        nei = list(neighbors(current))
        if len(nei) == 0:
            # unable to enumerate neighbors, likely not authorized
            break
        nxt = random.choice(nei)
        if (current, nxt) == cut or (nxt, current) == cut:
            break
        nodes.append(nxt)
        current = nxt
        logger.info('current node: %s', current)

        try:
            user = free_api("/users/show").GetUser(user_id=current)
        except twitter.TwitterError as e:
            logger.warning('Twitter Error: %s', e)
            break # unable to continue, likely a deleted account
        print(user.screen_name, user.followers_count, user.friends_count, user.protected, file=sys.stderr)
        if user.followers_count + user.friends_count > 300000 or user.protected:
            logger.warning('Stopping at %s due to user limit (%d neighbors) or protection (protected = %s)', current, user.followers_count + user.friends_count, user.protected)
            # stop here. breaking the rules but w/e. intersection super
            # unlikely past this point
            break
    return nodes

def mhrw(root):
    """Generator approach to a random walk. Does not stop."""
    current = root
    previous = root

    logger.info('starting MHRW from %s (neighbors: %d)', root, neighbor_count(root))

    while True:
        try:
            ws = neighbors(current)
        except twitter.TwitterError as e:
            logger.warning('Twitter Error: %s', e)
            current = previous
            continue

        if len(ws) == 0:
            # dud account
            current = previous
            continue

        p = np.random.uniform(0, 1)
        w = np.random.choice(list(ws))

        w_neighbors = neighbor_count(w)

        if w_neighbors == 0 or w_neighbors > 300000:
            # we would backtrack from w if we go there, just skip to the
            # next round
            # 
            # if the neighbor count is too high, we don't move there
            # because enumeration time would be excessive
            continue

        if w_neighbors * p <= len(ws):
            previous = current
            current = w
            yield current
