//! This module contains several alternate estimators that were tested for use in calculating
//! Edgecut Weights. None performed as well as EBGStop. They remain here so that the benchmark
//! showing this can continue to function.
use rand::Rng;
use crate::SamplingResult;
use online_moments::MomentEstimator;

/// Implements the Stopping Rule Algorithm (SRA) of Dagum et al. 2000
///
/// This produces a 1 - ε approximation of the mean of Z based on samples Z_i taken from
/// `sample_fn` which holds with probability 1 - δ.
///
/// However, when the true mean is exactly 0, this method **will not terminate!** (And as it
/// approaches 0, it will take proportionally longer to run)
pub fn stopping_rule_alg<F>(
    mut sample_fn: F,
    eps: f64,
    delta: f64,
) -> SamplingResult
where
    F: FnMut() -> f64,
{
    assert!(0.0 < eps && eps < 1.0 && 0.0 < delta && delta < 1.0);

    let lambda = std::f64::consts::E - 2.0;
    let ups = 4.0 * lambda * (2.0 / delta).ln() / eps.powi(2);
    let ups_1 = 1.0 + (1.0 + eps) * ups;

    #[allow(non_snake_case)]
    let mut N = 0.0;
    #[allow(non_snake_case)]
    let mut S = 0.0;

    let mut est = MomentEstimator::new(1);

    while S < ups_1 {
        assert!(
            N < 100_000.0,
            "SRA appears to be stuck, N = {}, S = {}",
            N,
            S
        );
        N += 1.0;
        let s = sample_fn();
        est.update(s);
        S += s;
    }

    SamplingResult {
        estimate: ups_1 / N,
        sample_dist: Some(est),
        n_samples: N as usize,
    }
}

pub fn gbas<F, R: Rng>(
    mut sample_fn: F,
    eps: f64,
    delta: f64,
    rng: &mut R,
) -> SamplingResult
where
    F: FnMut() -> f64,
{
    use rand::distributions::{Distribution, Exp1};
    let mut sum = 0.0;
    let mut r = 0.0;
    let k = 2.0 * eps.powi(-2) * (1.0 - 4.0 / 3.0 * eps).recip() * (2.0 * delta.recip()).ln();
    let dist = Exp1;
    let mut n_samples = 0;

    let mut dist_info = MomentEstimator::new(1);
    while sum < k {
        let si = sample_fn();
        sum += si;
        r += dist.sample(rng);
        dist_info.update(si);
        n_samples += 1;
    }

    SamplingResult {
        estimate: (k - 1.0) / r,
        sample_dist: Some(dist_info),
        n_samples,
    }
}

pub fn huber_jones_stopping<F, R: Rng>(
    mut sample_fn: F,
    eps: f64,
    delta: f64,
    rng: &mut R,
) -> SamplingResult
where
    F: FnMut() -> f64,
{
    use rand::distributions::{Distribution, Poisson, Uniform};
    let uniform = Uniform::new_inclusive(0.0, 1.0);

    fn bernoulli_mean_sample<F, R>(sample_fn: &mut F, dist: Uniform<f64>, rng: &mut R) -> usize
    where
        F: FnMut() -> f64,
        R: Rng,
    {
        let xi = sample_fn();
        let u = dist.sample(rng);

        if u <= xi {
            1
        } else {
            0
        }
    }

    fn bernoulli_var_sample<F, R>(sample_fn: &mut F, dist: Uniform<f64>, rng: &mut R) -> usize
    where
        F: FnMut() -> f64,
        R: Rng,
    {
        if rng.gen_bool(0.5) {
            let x1 = sample_fn();
            let x2 = sample_fn();
            let u = dist.sample(rng);

            if u <= (x1 - x2).powi(2) {
                1
            } else {
                0
            }
        } else {
            0
        }
    }

    fn gbas<F, R>(sample_fn: &mut F, dist: Uniform<f64>, rng: &mut R, k: f64) -> (f64, usize)
    where
        F: FnMut() -> f64,
        R: Rng,
    {
        use rand::distributions::Exp1;
        let mut sum = 0;
        let mut n = 0;

        while (sum as f64) < k {
            sum += bernoulli_mean_sample(sample_fn, dist, rng);
            n += 1;
        }

        let exp = Exp1;
        let r = (0..k as usize).map(|_| exp.sample(rng)).sum::<f64>();

        ((k + 2.0) / r, n)
    }

    fn ltsa<F>(sample_fn: &mut F, est: f64, n: usize, eps: f64, eps_0: f64, c2: f64) -> (f64, usize)
    where
        F: FnMut() -> f64,
    {
        fn psi(s: f64) -> f64 {
            let mut res = 0.0;
            if s >= 0.0 {
                res += (1.0 + s + s.powi(2) / 2.0).ln();
            }

            if s <= 0.0 {
                res -= (1.0 - s + s.powi(2) / 2.0).ln();
            }

            res
        }

        let estp = est / (1.0 - eps_0.powi(2));
        let alpha = eps / (c2 * estp);
        let result = (0..n)
            .map(|_| estp + psi(alpha * (sample_fn() - estp)) / alpha)
            .sum::<f64>()
            / n as f64;

        (result, n)
    }

    let eps_0 = eps.cbrt();
    let k = (2.0 * eps.powf(2.0 / 3.0) * (6.0 / delta).ln()).ceil();
    let (est, gbas_samples) = gbas(&mut sample_fn, uniform, rng, k);
    let pois = Poisson::new(2.0 * (3.0 / delta).ln() / (eps * est));
    let n_a = pois.sample(rng);

    let a = (0..n_a)
        .map(|_| bernoulli_var_sample(&mut sample_fn, uniform, rng))
        .sum::<usize>();
    let c1 = 2.0 * (3.0 / delta).ln();
    let c2 =
        (a as f64 / c1 + 0.5 + (a as f64 / c1 + 0.25).sqrt()) * (1.0 + eps_0).powi(2) * eps / est;

    let n = (2.0 * eps.powi(-2) * (6.0 / delta).ln() * c2 / (1.0 - eps_0)).ceil();

    let (est_final, ltsa_samples) = ltsa(&mut sample_fn, est, n as usize, eps, eps_0, c2);

    SamplingResult {
        estimate: est_final,
        n_samples: gbas_samples + ltsa_samples + n_a as usize,
        sample_dist: None,
    }
}