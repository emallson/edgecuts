import json
import random

import igraph
import networkx as nx
import numpy as np
from luigi import IntParameter, LocalTarget, Parameter, Task, WrapperTask

from .mixins import DictOutputMixin


def graph_path(g):
    return 'data/mono/{}.txt'.format(g)


def load_graph(name, relabel=True):
    with open(graph_path(name)) as f:
        # first line of these files is n, m
        # nx doesn't want that line
        g = nx.parse_edgelist([" ".join(e.split()[:2]) for i, e in enumerate(f) if i > 0], nodetype=int)
    if relabel:
        # relabel onto [0,n]
        nodes = sorted(g.nodes())
        mapping = dict(zip(nodes, range(0, g.number_of_nodes())))
        g = nx.relabel_nodes(g, mapping)
        return g, mapping
    else:
        return g


class LouvainShufflePart(DictOutputMixin, Task):
    LOG_PATH="results/louvain/partitions/{}.json"
    key = Parameter()
    graph = Parameter()
    seed = IntParameter(default=None)

    def run(self):
        g, base_mapping = load_graph(self.graph)
        np.random.seed(self.seed)

        inv_base_mapping = {j: i for i, j in base_mapping.items()}

        el = list(g.edges())
        idx = np.arange(len(el))
        np.random.shuffle(idx)

        ig = igraph.Graph.TupleList([el[i] for i in idx], directed=False)

        labels = ig.community_multilevel(return_levels=False)

        assert len(labels.membership) == g.number_of_nodes()
        ncut = sum(labels.crossing())

        result_labels = {
            # pylint: disable=unsubscriptable-object
            inv_base_mapping[ig.vs['name'][i]]: labels.membership[i]
            for i in range(g.number_of_nodes())
        }

        with self.output().open('w') as f:
            json.dump({
                "labels": result_labels,
                "ncut": ncut,
                "modularity": ig.modularity(labels.membership)
            }, f)

    def partitions(self):
        with self.output().open('r') as f:
            return json.load(f)['labels']

class LouvainShufflePart_Set(WrapperTask):
    key = 'fixed-set'
    seeds = [1143081386, 2886939892, 3001161128, 4019119586, 214205518, 678829054, 3392053739, 3971803777, 1111312795, 814033990, 3638557547, 4238134750, 3464357239, 3628749633, 3820337414, 3144578933, 1105871487, 4197854859, 3450405824, 1425305519, 215467736, 2089642889, 2092730011, 2787429044, 1659846766, 2275216632, 1729121758, 3177414710, 2647951106, 2172006808, 3672884484, 384862677, 2170008162, 2207234387, 3874187199, 663102724, 305532582, 873883556, 2974062200, 3542457076, 3000031927, 3640862175, 1512391006, 931553184, 2123448474, 239919199, 3649945884, 244073977, 3799896704, 1233462550, 2778793113, 25068051, 2553912941, 2095315597, 3911117405, 265694890, 446505109, 1215007349, 2677939647, 2039854891, 3227866321, 2110834096, 2906429157, 4268224958, 1748434055, 3572959701, 1052846819, 3898790722, 2856943372, 2307962255, 2066034202, 2722250777, 3973523897, 1377206397, 518384784, 733124238, 2544460954, 4074287611, 2649321249, 1946536387, 31717831, 475172006, 1294604890, 2198137656, 2508575137, 1854968773, 3772384320, 2648703888, 351437644, 3957074369, 1614182226, 1074170859, 1268020857, 3005216261, 1262703152, 3644739676, 2382384927, 797305369, 2753232315, 1955930359]

    graph = Parameter()
    npart = IntParameter(significant=False) # dummy parameter to make my life easier

    def requires(self):
        return [
            LouvainShufflePart(key=self.key, graph=self.graph, seed=s)
            for s in self.seeds
        ]
