#cython: language_level=3
from cython.view cimport array as cvarray
import numpy as np
from tqdm import tqdm, trange

def ecdf(double[:] steps, double[:] weights, long[:] output):
    """`steps` should be in ascending order. output should have the same shape as `steps`"""
    cdef size_t wix, stepix, num_weights, num_steps
    num_weights = weights.shape[0]
    num_steps = steps.shape[0]
    print(num_steps, num_weights)

    for wix in trange(num_weights):
        for stepix in range(num_steps-1, -1, -1):
            if steps[stepix] >= weights[wix]:
                output[stepix] += 1
            else:
                break


def calc_recip(long[:] sources, long[:] targets):
    cdef size_t ix
    recip = set()
    fwd = set()

    num_rows = sources.shape[0]
    for ix in trange(num_rows):
        canon = (min(sources[ix], targets[ix]), max(sources[ix], targets[ix]))
        if canon in fwd:
            recip.add(canon)
        else:
            fwd.add(canon)
    return recip, fwd


def recip_weights(set recip, long[:] sources, long[:] targets, double[:] weights):
    cdef size_t ix

    pos = []
    neg = []
    num_rows = sources.shape[0]
    for ix in trange(num_rows):
        canon = (min(sources[ix], targets[ix]), max(sources[ix], targets[ix]))
        if canon in recip:
            pos.append(weights[ix])
        else:
            neg.append(weights[ix])
    return pos, neg

def grouped_dists(long[:] sources, long[:] targets, double[:] weights):
    cdef size_t ix

    counts = {}
    mins = {}
    maxs = {}
    sums = {}

    def update(key, weight):
        if key not in counts:
            counts[key] = 1
            mins[key] = weight
            maxs[key] = weight
            sums[key] = weight
        else:
            counts[key] += 1
            mins[key] = min(mins[key], weight)
            maxs[key] = max(maxs[key], weight)
            sums[key] += weight

    num_rows = sources.shape[0]
    for ix in trange(num_rows):
        update(sources[ix], weights[ix])
        update(targets[ix], weights[ix])

    return counts, mins, maxs, sums
