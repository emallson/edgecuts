use hashbrown::{HashMap, HashSet};
use nom::branch::alt;
use nom::character::complete::*;
use nom::combinator::all_consuming;
use nom::number::complete::double;
use nom::sequence::tuple;
use nom::IResult;
use petgraph::prelude::*;
use std::io::{BufRead, BufReader};
use vec_graph::Graph;

fn header(i: &str) -> IResult<&str, (usize, usize)> {
    let (input, (n, _, m, _)) = tuple((digit1, multispace1, digit1, line_ending))(i)?;
    Ok((input, (n.parse().unwrap(), m.parse().unwrap())))
}

fn weighted_edge(i: &str) -> IResult<&str, (u32, u32, f64)> {
    let (input, (u, _, v, _, w, _)) = tuple((
        digit1,
        multispace1,
        digit1,
        multispace1,
        double,
        line_ending,
    ))(i)?;
    Ok((input, (u.parse().unwrap(), v.parse().unwrap(), w)))
}

fn unweighted_edge(i: &str) -> IResult<&str, (u32, u32, f64)> {
    let (input, (u, _, v, _)) = tuple((digit1, multispace1, digit1, line_ending))(i)?;
    Ok((input, (u.parse().unwrap(), v.parse().unwrap(), 1.0)))
}

pub fn load<R: std::io::Read>(
    reader: R,
    without_dups: bool,
) -> Result<Graph<u32, f64, Undirected>, Box<dyn std::error::Error>> {
    let mut bf = BufReader::new(reader);
    let mut line = String::new();
    bf.read_line(&mut line)?;
    let (_, (n, m)) = all_consuming(header)(&line).unwrap();

    let mut map = HashMap::new();
    let mut edgemap = HashSet::new();
    let mut g = Graph::<_, _, Undirected>::with_capacity(n, m);
    let mut i = 0;
    // should have m more lines
    loop {
        line.clear();
        let len = bf.read_line(&mut line)?;

        if len == 0 {
            break;
        }
        let (_, (u, v, w)) = all_consuming(alt((weighted_edge, unweighted_edge)))(&line)
            .expect(&format!("Failed to read edge {} (line {})", i, i + 1));

        let u = *map.entry(u).or_insert_with(|| g.add_node(u));
        let v = *map.entry(v).or_insert_with(|| g.add_node(v));

        if without_dups {
            let key = (u.min(v), u.max(v));
            if !edgemap.contains(&key) {
                g.add_edge(u, v, w);
                edgemap.insert(key);
            }
        } else {
            g.add_edge(u, v, w);
        }

        i += 1;
    }
    Ok(g)
}

#[cfg(test)]
mod test {
    use super::load;
    use std::fs::File;
    #[test]
    fn load_grqc() {
        use petgraph::visit::NodeCount;
        let g = load(&mut File::open("../data/mono/ca-GrQc.txt").unwrap(), false).unwrap();
        assert_eq!(g.node_count(), 5242);
        assert_eq!(g.edge_count(), 28980);
    }
}
